package ai.maum.sso.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import java.util.HashMap;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by kirk@mindslab.ai on 2020-09-15
 */
@ActiveProfiles("local")
@SpringBootTest
@RunWith(SpringRunner.class)
@Transactional
class Auth_ServiceTest {

    private final GoogleAuthService service;

    public Auth_ServiceTest(GoogleAuthService service) {
       this.service = service;
    }

    @Test
    @Rollback(true)
    void authorize() {
        HttpServletResponse response = new MockHttpServletResponse();
        String stateMaumHQ = "ca58d599-ee86-4173-bcf9-8014260ba4b2";
        String state = UUID.randomUUID().toString();
        String clientId = "cloud_minutes.local";
        String redirectUri = "redirectUri";
        String returnUri = service.authorize(response, stateMaumHQ, null, clientId, redirectUri, state);
        System.out.print(returnUri);
        assertNotNull(returnUri);
    }

    @Test
    @Rollback(true)
    void returnServiceCallback() {
        String state = "50b26e9c-cb3d-473f-a5a2-746be6c4c6cf";
        String ret = service.returnServiceCallback(state);
        System.out.print(ret);
        assertNotNull(ret);
    }

    @Test
    //@Rollback(true)
    void requestPublishingTokens() {
        String grantType = "authorization_code";
        String code = "9f359af6-14b1-4c3a-9d1f-4e8d882a3087";
        String refreshToken = "4be88f99-5b5f-47e7-abbf-12c091e2228b";
        HashMap<String, Object> map = service.requestPublishingTokens(grantType, "returnuri", code, refreshToken);
        System.out.println(map.toString());
        assertNotNull(map);
    }

}