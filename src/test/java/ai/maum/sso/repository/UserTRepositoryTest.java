package ai.maum.sso.repository;

import ai.maum.sso.domain.UserTEntity;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by kirk@mindslab.ai on 2020-09-14
 */
@ActiveProfiles("local")
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserTRepositoryTest {

    private final UserTRepository repository;

    public UserTRepositoryTest(UserTRepository repository) {
        this.repository = repository;
    }

    @Test
    @Rollback
    public void getUserTest() {
        UserTEntity entity = repository.findByUserNo(4177);
        assertNotNull(entity);
    }

}