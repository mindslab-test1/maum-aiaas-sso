package ai.maum.sso.service;

import ai.maum.sso.domain.SsoUserHistoryEntity;
import ai.maum.sso.domain.UserTEntity;
import ai.maum.sso.repository.SsoUserHistoryRepository;
import ai.maum.sso.repository.UserTRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class SupportService {

    private final UserTRepository userTRepository;
    private final SsoUserHistoryRepository ssoUserHistoryRepository;

    /**
     * 로그인한 사용자 정보 기록 - 2020. 11. 17 - LYJ
     * */
    public void recordLoginHistory(String email) {

        SsoUserHistoryEntity entity = new SsoUserHistoryEntity();
        entity.setEmail(email);

        LocalDateTime createDate = LocalDateTime.now();
        entity.setRegDate(createDate);
        ssoUserHistoryRepository.save(entity);
    }

    /**
     * Api ID, Key 발급 - 2020. 09. 28 - LYJ
     * */
    public ResponseEntity<String> createApiIdKey(String email, String name) throws Exception {
        Map<String, String> result = new HashMap<>();

        /* apiId가 중복될 경우 다시 실행 */
        do {
            UUID uuid = UUID.randomUUID();
            String apiId = email.split("@")[0] + uuid.toString().split("-")[0] + uuid.toString().split("-")[1];

            Map<String, String> usrMap = new HashMap<>();
            usrMap.put("apiId", apiId);
            usrMap.put("email", email);
            usrMap.put("name", name);

            result = addApiKey(usrMap);
        } while (result.get("message").equals("IdOverlap"));

        System.out.println("createApiIdKey result --> " + result.toString());

        if("Success".equals(result.get("message")))
            return new ResponseEntity<String>("success", HttpStatus.OK);
        else
            return new ResponseEntity<String>("fail", HttpStatus.BAD_REQUEST);
    }

    public Map<String, String> addApiKey(Map<String, String> usrMap) throws Exception {
        log.info(" @ addApiKey userMap --> " + usrMap.toString());

        String apiUrl = "https://api.maum.ai/admin/addClient";
        //String apiUrl = "https://dev-api.maum.ai/admin/addClient";
        Map<String, String> apiMap = sendApiPost(apiUrl, usrMap);

        return apiMap;
    }

    private Map<String, String> sendApiPost(String sendUrl, Map<String, String> usrMap) throws Exception{
        StringBuffer outResult = new StringBuffer();
        StringBuffer result = new StringBuffer();

        Map<String, String> apiMap = new HashMap<>();
        UserTEntity entity = userTRepository.findByApiId(usrMap.get("apiId"));

        if(null != entity) {
            apiMap.put("message", "IdOverlap");
            return apiMap;
        }

        /* api 등록 */
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(usrMap);

            URL url = new URL(sendUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");

            OutputStream os = conn.getOutputStream();
            os.write(jsonValue.getBytes("UTF-8"));
            os.flush();

            int nResponseCode = conn.getResponseCode();

            /* api 등록 */
            if (nResponseCode == HttpURLConnection.HTTP_OK) { // 200 성공
                log.info("httpConnection success...");
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
                String inputLine;

                while ((inputLine = br.readLine()) != null)
                {
                    outResult.append(inputLine);
                    try {
                        result.append(new String(URLDecoder.decode(inputLine, "UTF-8")));
                    } catch (Exception ex) {
                        result.append(inputLine);
                    }
                }

                log.info(result.toString());
                log.info(outResult.toString());
                br.close();
            } else {
                throw new Error("httpConnection error...");
            }
            conn.disconnect();
        }catch (Exception e){
            apiMap.put("message", "fail");
            log.info("api 서버 에러");
            return apiMap;
        }

        /* apiKey 가져옴 */
        JsonParser parser = new JsonParser();
        Object obj = parser.parse(result.toString());
        JsonObject apiJson = (JsonObject)obj;
        String message = apiJson.get("message").toString().replace("\"", "");


        /* db에 api 값 저장 */
        try {
            if (message.equals("Success")) {
                String apiKey = apiJson.getAsJsonObject("data").get("apiKey").toString().replace("\"", "");
                log.info("apiKey : " + apiKey + ", " + "message : " + message);

                apiMap.put("message", "Success");
                apiMap.put("id", usrMap.get("apiId"));
                apiMap.put("key", apiKey);

                UserTEntity userTEntity = userTRepository.findByEmail( usrMap.get("email") );
                userTEntity.setApiId(usrMap.get("apiId"));
                userTEntity.setApiKey(apiKey);
                userTEntity.setEmail(usrMap.get("email"));

                userTRepository.save(userTEntity);

            } else {
                apiMap.put("message", "fail");
            }
        }catch(Exception e){
            log.debug("DB error...");
            apiMap.put("message", "fail");
        }

        return apiMap;
    }
}
