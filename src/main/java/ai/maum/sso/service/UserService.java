package ai.maum.sso.service;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.domain.UserTEntity;
import ai.maum.sso.repository.UserTRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserTRepository userTRepository;

    public HashMap<String, Object> getUserStatus(String email) {

        UserTEntity userTEntity = userTRepository.findByEmail(email);

        // 사용자 정보가 없을 경우, Exception 발생
        if (userTEntity == null) throw new RuntimeException("");

        // 사용자 정보가 있을 경우
        int userStatus = userTEntity.getStatus();
        HashMap<String, Object> resultMap = new HashMap<>();
        if (userStatus == ConstDef.USER_SUBSCRIBE_STATUS_INIT) {    // INIT
            if(userTEntity.getPrivacyAgree() == 1) {    // 개인 정보 동의를 한 회원의 경우 무료 사용 기간을 보내고 있는 고객
                resultMap.put("status", "payment");
            } else {
                resultMap.put("status", "free");
            }
            resultMap.put("detail", "INIT");

        } else if (userStatus == ConstDef.USER_SUBSCRIBE_STATUS_FREE) { // FREE

            resultMap.put("status", "payment");
            resultMap.put("detail", "FREE");

        } else if (userStatus == ConstDef.USER_SUBSCRIBE_STATUS_SUBSCRIBED) {    // SUBSCRIBED

            resultMap.put("status", "payment");
            resultMap.put("detail", "SUBSCRIBED");

        } else if (userStatus == ConstDef.USER_SUBSCRIBE_STATUS_UNSUBSCRIBING) { // UNSUBSCRIBING

            resultMap.put("status", "payment");
            resultMap.put("detail", "UNSUBSCRIBING");

        } else if (userStatus == ConstDef.USER_SUBSCRIBE_STATUS_UNSUBSCRIBED) {  // UNSUBSCRIBED

            resultMap.put("status", "free");
            resultMap.put("detail", "UNSUBSCRIBED");

        } else {
            // TBD
        }

        return resultMap;
    }
}
