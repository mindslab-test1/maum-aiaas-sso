package ai.maum.sso.service;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.common.MailSender;
import ai.maum.sso.common.Utils;
import ai.maum.sso.common.data.CommonMsg;
import ai.maum.sso.common.data.CommonResponse;
import ai.maum.sso.domain.*;
import ai.maum.sso.dto.UserDto;
import ai.maum.sso.repository.*;
import ai.maum.sso.utils.CookieUtil;
import ai.maum.sso.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class MaumRestService {

    private final String SIGNUP_CALLBACK___PATH = "/hq/oauth/signup-member-callback";
    private final String MAUM_AI___PAYMENT_PATH = "/maum/user/info";

    private final OAuthTokenRepository oAuthTokenRepository;
    private final SsoUserRepository ssoUserRepository;
    private final SsoAuthLogRepository ssoAuthLogRepository;
    private final UserTRepository userTRepository;
    private final OAuthClientRepository oAuthClientRepository;
    private final MailSender mailSender;
    private final TemplateEngine templateEngine;
    private final RedisTemplate redisTemplate;
    private final CookieUtil cookieUtil;
    private final JwtTokenUtil jwtTokenUtil;

    private final GoogleAuthService googleAuthService;
    private final ModelMapperService modelMapperService;

    private final SupportService supportService;

    @Value("${my.domain}")
    private String DOMAIN;

    @Value("${url.maum-ai}")
    private String URL___MAUM_AI;

    public OAuthTokenEntity checkState(String state) {
        OAuthTokenEntity tokenEntity = oAuthTokenRepository.findByState(state);
        return tokenEntity;
    }

    /** ------------------------------------------------------------------------------------------------------------ */
    /** 로그인 서비스 관련 - 2020. 09. 16 - LYJ */
    /** ------------------------------------------------------------------------------------------------------------ */
    /*
     ** 로그인 요청에 대한 처리
     */
    public CommonResponse<Object> authorize(String email, String userPw, String stateMaumHQ, String client_id, String redirect_uri, String state, HttpServletResponse response) {

        OAuthTokenEntity tokenEntity;

        /* 쿠키가 존재하면, 재로그인으로 바로 콜백 호출 */
        if (stateMaumHQ != null && (tokenEntity = oAuthTokenRepository.findByState(stateMaumHQ)) != null && Utils.isExpiredTime(tokenEntity.getRefreshExpireDatetime()) == false) {
            log.info("=====>> SsoService.authorize : 자체 재로그인!");

            CommonResponse<Object> resp = null;
            HashMap<String, Object> respData = null;

            resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
            resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);

            respData.put("email", email);
            respData.put("code", tokenEntity.getAuthorizationCode());
            respData.put("state", state);

            resp.setData(respData);

            return resp;
        }

        /* 쿠키가 존재하지 않으면, 초기 로그인으로 구글 로그인 절차 수행 */
        {
            log.info("=====>> SsoService.authorize : 자체 초기 로그인!");

            // 존재하지 않을 시, 새로운 UUID 생성
            String maumHqState = UUID.randomUUID().toString();

            Cookie myCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, maumHqState);
            myCookie.setDomain("maum.ai");
            myCookie.setMaxAge(24 * 60 * 60);
            myCookie.setPath("/");
            response.addCookie(myCookie);

            // 계정 인증
            CommonResponse<Object> checkUser = checkMaumUser(email, userPw, client_id, redirect_uri, maumHqState);

            return checkUser;
        }
    }

    public CommonResponse<Object> checkMaumUser(String email, String userPw, String client_id, String return_uri, String state) {

        SsoUserEntity checkUser = ssoUserRepository.findByEmailAndPassword(email, userPw);

        CommonResponse<Object> resp = new CommonResponse<>();
        HashMap<String, String> respData = new HashMap<>();

        respData.put("email", email);
        respData.put("state", state);

        // 사용자 정보가 존재할 때
        if (checkUser != null) {

            // OAUTH_TOKEN table insert (code, state)
            OAuthTokenEntity entity = new OAuthTokenEntity();
            entity.setState(state);
            entity.setEmail(email);
            entity.setClientId(client_id);

            String code = UUID.randomUUID().toString();
            entity.setAuthorizationCode(code);
            entity.setAccessToken(UUID.randomUUID().toString());
            entity.setRefreshToken(UUID.randomUUID().toString());

            LocalDateTime createDate = LocalDateTime.now();
            entity.setCreateDatetime(createDate);
            entity.setUpdateDatetime(createDate);
            oAuthTokenRepository.save(entity);

            respData.put("return_uri", procMaumCallback(code, state, return_uri));

            resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
            resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);

            respData.put("code", code);

            resp.setData(respData);

        } else { // 사용자 정보가 존재하지 않을 때
            resp.setCode(CommonMsg.ERR_CODE_NOT_FOUND);
            resp.setMsg(CommonMsg.ERR_MSG_NOT_FOUND);

            respData.put("code", null);

            resp.setData(respData);
        }
        return resp;
    }

    public String procMaumCallback(String code, String state, String redirect_uri) {

        String email = null;

        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if (oAuthTokenEntity != null) {
            email = oAuthTokenEntity.getEmail();
            LocalDateTime updateDate = LocalDateTime.now();
            oAuthTokenEntity.setUpdateDatetime(updateDate);
            oAuthTokenEntity.setAccessExpireDatetime(updateDate.plusHours(1));
            oAuthTokenEntity.setRefreshExpireDatetime(updateDate.plusHours(3));
            oAuthTokenRepository.save(oAuthTokenEntity);
        } else {
            throw new RuntimeException("state not exist:state=" + state);
        }

        /* maum.ai 서비스가 활성화 되어 있지 않으면 회원 등록 수행 */
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        log.info("# maum.ai member(maum)'s status = {} / {}", email, (userTEntity == null ? null : userTEntity.toString()));

        if ((oAuthClientEntity != null && oAuthClientEntity.getFlagCreateMember() == 1)
                && (userTEntity == null
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___INIT
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___UNSUBSCRIBED
                || userTEntity.getPrivacyAgree() == 0) ) {
            String callback_uri = DOMAIN + SIGNUP_CALLBACK___PATH + "?state=" + state;
            String return_uri = String.format("%s%s?email=%s&callback_uri=%s", URL___MAUM_AI, MAUM_AI___PAYMENT_PATH, email, callback_uri);

            return return_uri;
        } else {
            /* 이미 등록된 고객이면, 서비스 콜백으로 리다이렉트 한다. */
            return returnMaumServiceCallback(state, redirect_uri);
        }
    }

    /*
     ** 서비스 콜백으로 접근 코드 반환
     */
    public String returnMaumServiceCallback(String state, String redirect_uri) {

        log.debug("returnMaumServiceCallback");

        /* 토큰 정보 조회 */
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if(oAuthTokenEntity == null) {
            log.error("# procMaumCallback@oauthMapper.getTokenByState({}) fail", state);
        }

        /* RedirectUri 조회 */
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        if(oAuthClientEntity == null) {
            log.error("# procMaumCallback@oauthMapper.getClientInfo({}) fail", oAuthClientEntity.getClientId());
        }
        return redirect_uri + "?state=" + state + "&code=" + oAuthTokenEntity.getAuthorizationCode();
    }


    /* ------------------------------------------------------------------------------------------------------------- */
    /* 통합로그인 2.0 - 추후 1.0 제거 예정  */

    public CommonResponse<Object> ssoAuthorize(String email, String userPw, String stateMaumHQ, String client_id, String redirect_uri, String state, HttpServletResponse response) {
        String returnUri = null;
        OAuthTokenEntity tokenEntity = null;

        /* 쿠키가 존재하면, 초기 로그인으로 구글 로그인 절차 수행 */
        log.info("=====>> SsoService.ssoAuthorize : 자체 로그인!");

        // 존재하지 않을 시, 새로운 UUID 생성
        String maumHqState = UUID.randomUUID().toString();

        Cookie myCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, maumHqState);
        myCookie.setDomain("maum.ai");
        myCookie.setMaxAge(24 * 60 * 60);
        myCookie.setPath("/");
        response.addCookie(myCookie);

        // 계정 인증
        CommonResponse<Object> checkUser = checkSsoMaumUser(email, userPw, client_id, redirect_uri, maumHqState, response);

        return checkUser;
    }

    public CommonResponse<Object> checkSsoMaumUser(String email, String userPw, String client_id, String return_uri, String state, HttpServletResponse response) {

        SsoUserEntity checkUser = ssoUserRepository.findByEmailAndPassword(email, userPw);

        CommonResponse<Object> resp = new CommonResponse<>();
        HashMap<String, String> respData = new HashMap<>();

        respData.put("email", email);
        respData.put("state", state);

        // 사용자 정보가 존재할 때
        if (checkUser != null) {

            // OAUTH_TOKEN table insert (code, state)
            OAuthTokenEntity entity = new OAuthTokenEntity();
            entity.setState(state);
            entity.setEmail(email);
            entity.setClientId(client_id);

            String code = UUID.randomUUID().toString();
            entity.setAuthorizationCode(code);
            entity.setAccessToken(UUID.randomUUID().toString());
            entity.setRefreshToken(UUID.randomUUID().toString());

            LocalDateTime createDate = LocalDateTime.now();
            entity.setCreateDatetime(createDate);
            entity.setUpdateDatetime(createDate);
            oAuthTokenRepository.save(entity);

            respData.put("return_uri", procSsoMaumCallback(code, state, return_uri));

            resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
            resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);

            respData.put("code", code);

            resp.setData(respData);

            /* SSO 2.0 ------------------------------------------------------------------------------------------- */

            UserTEntity userTEntity = userTRepository.findByEmail(email);
            if(userTEntity.getPrivacyAgree() == 0) {
                log.debug("Skipping JWT cookie generation");
            } else {
                // 사용자의 결제 금액에 따른 사용 가능 서비스 조회
                int product = googleAuthService.getUserProduct(email);
                Map<String, Object> usableSrvMap = googleAuthService.getUsableServiceList(product, email);

                // Generate Access/Refresh-Token based on JWT(Json Web Token)
                String accessToken = jwtTokenUtil.generateAccessToken(email, usableSrvMap);
                String refreshToken = jwtTokenUtil.generateRefreshToken(email, usableSrvMap);

                // TODO: Store redis refresh-token
                redisTemplate.opsForValue().set(email + "_refresh_token", refreshToken);
                log.info(redisTemplate.opsForValue().get(email + "_refresh_token").toString());

                // Store Cookie for tokens(access/refresh) in ServletHttpResponse
                cookieUtil.generateCookieForJwtToken(accessToken, refreshToken, response);
            }
            /* SSO 2.0 ------------------------------------------------------------------------------------------- */

        } else { // 사용자 정보가 존재하지 않을 때
            resp.setCode(CommonMsg.ERR_CODE_NOT_FOUND);
            resp.setMsg(CommonMsg.ERR_MSG_NOT_FOUND);

            respData.put("code", null);

            resp.setData(respData);
        }
        return resp;
    }

    public String procSsoMaumCallback(String code, String state, String return_uri) {

        String email = null;

        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if (oAuthTokenEntity != null) {
            email = oAuthTokenEntity.getEmail();
            LocalDateTime updateDate = LocalDateTime.now();
            oAuthTokenEntity.setUpdateDatetime(updateDate);
            oAuthTokenEntity.setAccessExpireDatetime(updateDate.plusHours(1));
            oAuthTokenEntity.setRefreshExpireDatetime(updateDate.plusHours(3));
            oAuthTokenRepository.save(oAuthTokenEntity);
        } else {
            throw new RuntimeException("state not exist:state=" + state);
        }

        /* maum.ai 서비스가 활성화 되어 있지 않으면 회원 등록 수행 */
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        log.info("# maum.ai member(maum)'s status = {} / {}", email, (userTEntity == null ? null : userTEntity.toString()));

        if ((oAuthClientEntity != null && oAuthClientEntity.getFlagCreateMember() == 1)
                && (userTEntity == null
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___INIT
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___UNSUBSCRIBED
                || userTEntity.getPrivacyAgree() == 0) ) {
            String callback_uri = DOMAIN + SIGNUP_CALLBACK___PATH + "?state=" + state;
            String return_url = String.format("%s%s?email=%s&callback_uri=%s", URL___MAUM_AI, MAUM_AI___PAYMENT_PATH, email, callback_uri);

            return return_url;
        } else {
            // 이미 등록된 고객이면, 서비스 콜백으로 리다이렉트
            return return_uri;
        }
    }

    /* ------------------------------------------------------------------------------------------------------------- */



    /** ------------------------------------------------------------------------------------------------------------ */
    /** 회원가입 서비스 관련 - 2020. 09. 23 - LYJ */
    /** ------------------------------------------------------------------------------------------------------------ */

    /*
    * 이메일로 사용자 조회
    * */
    public SsoUserEntity checkDuplicateEmail(String email) {
        //log.info(" @ Hello checkDuplicateEmail ! ");

        SsoUserEntity ssoUserEntity = ssoUserRepository.findByEmail(email);
        return ssoUserEntity;
    }
    
    /*
    * maum.ai User_t 이메일 조회
    * */
    public UserTEntity checkUserEmail(String email) {
        //log.info(" @ Hello checkUserEmail ! ");
        
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        return userTEntity;
    }

    /*
    * 인증 코드 메일로 전송
    * */
    @Transactional
    public void sendCertMail(String userEmail, String type) throws MessagingException {

        // 인증 코드 생성
        String code = numberGen(6, 2);
        log.info(" @ sendCertMail - userEmail: {}, code: {}, type : {}", userEmail, code, type);

        // type에 따른 구분 (회원가입 : register, 비밀번호 찾기 : password)
        String authType = "";
        if("register".equalsIgnoreCase(type)) {
            authType = ConstDef.AUTH_TYPE_SIGN;
        } else if ("password".equalsIgnoreCase(type)) {
            authType = ConstDef.AUTH_TYPE_PASSWORD;
        } else {
            /* N/A */
        }

        // 메일 내용에 코드 추가
        final Context ctx = new Context();
        ctx.setVariable("code", code);
        final String htmlContent = this.templateEngine.process("/mail/emailAuth.html", ctx);

        // 인증 전 코드 존재 시, 만료 처리
        List<SsoAuthLogEntity> entityList = ssoAuthLogRepository.findByEmailAndAuthStatusAndAuthType(userEmail, ConstDef.AUTH_STATUS_READY, authType);
        if( entityList != null ) {
            for(int i=0 ; i< entityList.size() ; i++) {
                SsoAuthLogEntity tempEntity = entityList.get(i);
                tempEntity.setAuthStatus(ConstDef.AUTH_STATUS_EXPIRE);
                ssoAuthLogRepository.save(tempEntity);
            }
        }

        // 메일 전송
        mailSender.postMail(userEmail, "[마인즈랩] 이메일 인증 부탁드립니다.", htmlContent, "sendmail@mindslab.ai");

        // table에 인증키 insert
        LocalDateTime createDate = LocalDateTime.now();

        SsoAuthLogEntity entity = new SsoAuthLogEntity();
        entity.setEmail(userEmail);
        entity.setAuthType(authType);
        entity.setAuthStatus(ConstDef.AUTH_STATUS_READY); // 인증 전
        entity.setAuthKey(code);
        entity.setExpireDate(createDate.plusMinutes(30));
        entity.setRegDate(createDate);
        entity.setMdfDate(createDate);

        ssoAuthLogRepository.save(entity);
    }

    /*
    * 인증 코드 일치 및 유효 시간 확인
    * */
    public CommonResponse<Object> checkCertCode(String userEmail, String userCode, String type) {

        CommonResponse<Object> response = new CommonResponse<>();
        log.info("email : {}, code : {}, type : {}", userEmail, userCode, type);

        String authType="";
        if("register".equalsIgnoreCase(type)) {
            authType = ConstDef.AUTH_TYPE_SIGN;
        } else if ("password".equalsIgnoreCase(type)) {
            authType = ConstDef.AUTH_TYPE_PASSWORD;
        } else {
            /* N/A */
        }

        //DB에서 이메일과 코드로 조회
        SsoAuthLogEntity entity = ssoAuthLogRepository.findByEmailAndAuthKeyAndAuthStatusAndAuthType(userEmail, userCode, ConstDef.AUTH_STATUS_READY, authType);

        if(entity != null) {    // 조회 O
            // 유효시간 조회
            LocalDateTime now = LocalDateTime.now();
            if(entity.getExpireDate().isAfter(now)) {

                entity.setAuthStatus(ConstDef.AUTH_STATUS_COMPLETE);
                entity.setCertDate(now);
                entity.setMdfDate(now);
                ssoAuthLogRepository.save(entity);

                response.setCode(CommonMsg.ERR_CODE_SUCCESS);
                response.setMsg(CommonMsg.ERR_MSG_SUCCESS);
            } else {
                log.debug(" @ checkCertCode - userEmail&code is correct. ");
                response.setCode(CommonMsg.ERR_CODE_TIMEOUT);
                response.setMsg(CommonMsg.ERR_MSG_TIMEOUT);
            }
        } else {    // 조회 X
            response.setCode(CommonMsg.ERR_CODE_NOT_FOUND);
            response.setMsg(CommonMsg.ERR_MSG_NOT_FOUND);
        }
        return response;
    }

    /*
    * 회원가입
    * */
    @Transactional
    public CommonResponse<Object> registerUser(String email, String code, String password, String name, String phone, String company, String registerPath, int marketingAgree) throws Exception {

        CommonResponse<Object> result = new CommonResponse<>();
        HashMap<String, String> resultData = new HashMap<>();

        // 이메일 중복 검사 (SSO_User table, maum.ai User_T table)
        SsoUserEntity checkSsoUserEntity = ssoUserRepository.findByEmail(email);
        UserTEntity checkUserTEntity = userTRepository.findByEmail(email);

        if( (checkSsoUserEntity != null) || (checkUserTEntity != null) ){
            result.setCode(CommonMsg.ERR_CODE_EMAIL);
            result.setMsg(CommonMsg.ERR_MSG_EMAIL);
            result.setData(null);

            return result;
        }

        log.debug(email);
        log.debug(password);
        log.debug(name);
        log.debug(phone);
        log.debug(company);
        log.debug(registerPath);
        log.debug(String.valueOf(marketingAgree));

        // SSO_USER table insert
        SsoUserEntity ssoUserEntity = new SsoUserEntity();
        ssoUserEntity.setEmail(email);
        ssoUserEntity.setPassword(password);
        ssoUserEntity.setName(name);
        ssoUserEntity.setPhone(phone);
        ssoUserEntity.setRegisterPath(registerPath);
        ssoUserEntity.setCompany(company);
        ssoUserEntity.setTermsAgree(1);
        ssoUserEntity.setPrivacyAgree(1);
        ssoUserEntity.setMarketingAgree(marketingAgree);
        ssoUserEntity.setAccountStatus(ConstDef.ACCOUNT_STATUS_ACTIVATE);

        LocalDateTime createDate = LocalDateTime.now();
        ssoUserEntity.setRegDate(createDate);
        ssoUserEntity.setMdfDate(createDate);
        ssoUserEntity.setUpdatePasswordDate(createDate);
        ssoUserEntity.setPrivacyAgreeDate(createDate);
        ssoUserEntity.setLastLogin(createDate);

        ssoUserRepository.save(ssoUserEntity);
        log.debug(" @@ registerUser -- SsoUserRepository save complete !");

        // maum.ai USER_T table insert
        UserTEntity userTEntity = new UserTEntity();
        userTEntity.setName(name);
        userTEntity.setId(email.split("@")[0]);
        userTEntity.setPassword(password);
        userTEntity.setEmail(email);
        userTEntity.setPhone(phone);
        userTEntity.setProduct(3);  // Business - 99000원
        userTEntity.setEnabled(1);
        userTEntity.setAuthority("ROLE_USER");
        userTEntity.setPrivacyAgree(1);
        userTEntity.setPrivacyDate(createDate);
        userTEntity.setActive(1);
        userTEntity.setCreateDate(createDate);
        userTEntity.setCompany(company);
        userTEntity.setMarketingAgree(marketingAgree);
        userTEntity.setMarketingDate(createDate);
        userTEntity.setStatus(ConstDef.USER_SUBSCRIBE_STATUS_INIT);
        userTEntity.setRegisterPath(registerPath);
        userTEntity.setPayCount(0);
        userTEntity.setChannel("maum");

        userTRepository.save(userTEntity);
        log.debug(" @@ registerUser -- UserTEntity save complete !");

        // API ID, Key 발급 -----------------------------------
        ResponseEntity<String> createApiInfo =  supportService.createApiIdKey(email, name);
        log.debug("createApiInfo " + email + "'s result ==> " + createApiInfo);

        // 인증된 이메일인지 한 번 더 확인
        SsoAuthLogEntity ssoAuthLogEntity = ssoAuthLogRepository.findByEmailAndAuthStatusAndAuthTypeAndAuthKeyAndCertDateIsNotNull(email, ConstDef.AUTH_STATUS_COMPLETE, ConstDef.AUTH_TYPE_SIGN, code);

        if(ssoAuthLogEntity != null) {

            result.setCode(CommonMsg.ERR_CODE_SUCCESS);
            result.setMsg(CommonMsg.ERR_MSG_SUCCESS);

            resultData.put("email", ssoUserEntity.getEmail());
            resultData.put("name", ssoUserEntity.getName());
            resultData.put("phone", ssoUserEntity.getPhone());
            result.setData(resultData);

        } else {

            result.setCode(CommonMsg.ERR_CODE_FAILURE);
            result.setMsg(CommonMsg.ERR_MSG_FAILURE);
            result.setData(null);

        }
        return result;
    }

    /*
    * 비밀번호 변경
    * */
    @Transactional
    public CommonResponse<Object> changePW(String email, String newPassword) {

        CommonResponse<Object> result = new CommonResponse<>();

        SsoUserEntity ssoUserEntity = ssoUserRepository.findByEmail(email);
        ssoUserEntity.setPassword(newPassword);
        ssoUserRepository.save(ssoUserEntity);

        result.setCode(CommonMsg.ERR_CODE_SUCCESS);
        result.setMsg(CommonMsg.ERR_MSG_SUCCESS);

        return result;
    }

    @Transactional
    public Page<UserDto> userList(String type, String sch, Pageable pageable) {

        Page<UserTSimpleInterface> list = null;

        if("EMAIL".equalsIgnoreCase(type)) {
            list = userTRepository.findAllByEmailContaining(sch, pageable);
        }
        else if ("NAME".equalsIgnoreCase(type)) {
            list = userTRepository.findAllByNameContaining(sch, pageable);
        }
        else {
            list = userTRepository.findAllByEmailContainingOrNameContaining(sch, sch, pageable);
        }

        return list.map(user -> modelMapperService.map(user, UserDto.class));
    }

    /**
     * 전달된 파라미터에 맞게 난수를 생성한다
     * @param len : 생성할 난수의 길이
     * @param dupCd : 중복 허용 여부 (1: 중복허용, 2:중복제거)
     *
     * Created by 닢향
     * http://niphyang.tistory.com
     */
    public static String numberGen(int len, int dupCd) {

        Random rand = new Random();
        String numStr = ""; //난수가 저장될 변수

        for(int i=0;i<len;i++) {

            //0~9 까지 난수 생성
            String ran = Integer.toString(rand.nextInt(10));

            if(dupCd==1) { //중복 허용시 numStr에 append
                numStr += ran;
            }else if(dupCd==2) {
                //중복을 허용하지 않을시 중복된 값이 있는지 검사
                if(!numStr.contains(ran)) {
                    //중복된 값이 없으면 numStr에 append
                    numStr += ran;
                }else {
                    //생성된 난수가 중복되면 루틴을 다시 실행
                    i-=1;
                }
            }
        }
        return numStr;
    }
}