package ai.maum.sso.service;

import ai.maum.sso.common.data.CommonMsg;
import ai.maum.sso.common.data.CommonResponse;
import ai.maum.sso.domain.OAuthTokenEntity;
import ai.maum.sso.repository.OAuthTokenRepository;
import ai.maum.sso.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class OAuth2TokenService {
    private final RedisTemplate redisTemplate;
    private final JwtTokenUtil jwtTokenUtil;

    private final GoogleAuthService googleAuthService;
    private final OAuthTokenRepository oAuthTokenRepository;

    public String refreshAccessToken(String requestRefreshToken) {
        String email = jwtTokenUtil.extractEmail(requestRefreshToken);

        // [Check refresh_token]
        // whether expired in redis
        Optional.ofNullable(redisTemplate.opsForValue().get(email + "_refresh_token"))
                .orElseThrow(() -> new RuntimeException("Not found refresh_token in repository"));

        // whether (valid, expired...) in request value
        jwtTokenUtil.validateToken(requestRefreshToken);

        int product = googleAuthService.getUserProduct(email);

        Map<String, Object> usableSrvMap = googleAuthService.getUsableServiceList(product, email);

        return jwtTokenUtil.generateAccessToken(email, usableSrvMap);
    }

    public CommonResponse<Object> validateCode(String state, String code) {
        OAuthTokenEntity tokenEntity = oAuthTokenRepository.findByStateAndAuthorizationCode(state, code);

        boolean validateFlag = false;
        CommonResponse<Object> response = new CommonResponse<>();

        if(tokenEntity != null) {
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime createTime = tokenEntity.getCreateDatetime();

            System.out.println("valid = " + createTime.plusMinutes(60).isAfter(now));
            System.out.println("createTime = " + createTime);
            System.out.println("creatTime + 60 = " + createTime.plusMinutes(60));
            System.out.println("now = " + now);

            if(createTime.plusMinutes(60).isAfter(now)) {
                validateFlag = true;
            }

            HashMap<String, Object> dataMap = new HashMap<>();
            dataMap.put("createCodeTime", createTime.toString());
            dataMap.put("valid", validateFlag);

            response.setCode(CommonMsg.ERR_CODE_SUCCESS);
            response.setMsg(CommonMsg.ERR_MSG_SUCCESS);
            response.setData(dataMap);

        } else {
            response.setCode(CommonMsg.ERR_CODE_NO_MATCHING_DATA);
            response.setMsg(CommonMsg.ERR_MSG_NO_MATCHING_DATA);
            response.setData(null);
        }

        return response;
    }
}
