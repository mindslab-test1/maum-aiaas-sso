package ai.maum.sso.service;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.domain.*;
import ai.maum.sso.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShopifyService {

    private final ShopifyLicenseRepository shopifyLicenseRepository;
    private final ShopifyLicenseManagementRepository shopifyLicenseManagementRepository;
    private final ShopifyLicenseDuplicateProtectionRepository shopifyLicenseDuplicateProtectionRepository;
    private final ShopifyCodeRepository shopifyCodeRepository;
    private final ShopifyCodeHistoryRepository shopifyCodeHistoryRepository;

    /* -------------------------------------------------------------------------------------------------------------- */
    /* 상품 코드 관련 API */
    /* -------------------------------------------------------------------------------------------------------------- */
    /** 구매한 고객의 상품 코드 등록 - 2021. 06. 04 - LYJ */
    @Transactional
    public int insertCode(String code, String email, String product, String model, String limit) throws Exception {
        log.info(" @ Hello ShopifyService.insertCode ! ");
        log.debug("insertCode: code = {}, email = {}, product = {}, model = {}, limit = {}", code, email, product, model, limit);

        if(checkCode(code) == 0) {
            return -1;
        }

        ShopifyCodeEntity entity = new ShopifyCodeEntity();
        entity.setCode(code);
        entity.setEmail(email);
        entity.setProduct(product);
        entity.setModel(model);
        entity.setLimit(limit);
        entity.setStatus(ConstDef.STORE_PRODUCT_BUY);

        ShopifyCodeEntity result = shopifyCodeRepository.save(entity);

        //history 기록
        ShopifyCodeHistoryEntity historyEntity = new ShopifyCodeHistoryEntity();
        historyEntity.setCode(code);
        historyEntity.setEmail(email);
        historyEntity.setProduct(product);
        historyEntity.setModel(model);
        historyEntity.setLimit(limit);
        historyEntity.setStatus(ConstDef.STORE_PRODUCT_BUY);

        ShopifyCodeHistoryEntity resultHistory = shopifyCodeHistoryRepository.save(historyEntity);

        if( (result != null) && (resultHistory != null) ) {
            return 1;
        } else {
            return 0;
        }
    }

    /** 만료되지 않은 상품 코드 조회 - 2021. 06. 04 - LYJ */
    public List<ShopifyCodeEntity> getValidCodeList(String email) throws Exception {
        log.info(" @ Hello ShopifyService.getValidCodeList ! ");
        log.debug("getValidCodeList: email = {}", email);

        List<ShopifyCodeEntity> result = shopifyCodeRepository.findByEmailAndStatus(email, ConstDef.STORE_PRODUCT_BUY);
        return result;
    }

    /** 상품 코드 status 변경 - 2021. 06. 04 - LYJ */
    @Transactional
    public int updateCodeStatus(String email, String code, String product, String model, String limit, String status) throws Exception {
        log.info(" @ Hello ShopifyService.updateProductCodeStatus ! ");
        log.debug("updateProductCodeStatus: code = {}, email = {}, product = {}, model = {}, limit = {}, status = {}", code, email, product, model, limit, status);

        ShopifyCodeEntity codeEntity = shopifyCodeRepository.findByCodeAndEmailAndProductAndModelAndLimit(code, email, product, model, limit);
        if(codeEntity == null) {
            return -1;
        }
        codeEntity.setStatus(status);
        ShopifyCodeEntity updateEntity = shopifyCodeRepository.save(codeEntity);

        //history 기록
        ShopifyCodeHistoryEntity historyEntity = new ShopifyCodeHistoryEntity();
        historyEntity.setCode(code);
        historyEntity.setEmail(email);
        historyEntity.setProduct(product);
        historyEntity.setModel(model);
        historyEntity.setLimit(limit);
        historyEntity.setStatus(status);

        ShopifyCodeHistoryEntity resultHistory = shopifyCodeHistoryRepository.save(historyEntity);

        if( (updateEntity != null) && (resultHistory != null) ) {
            return 1;
        } else {
            return 0;
        }
    }

    /** 상품 코드가 존재하는지 체크 */
    public int checkCode(String code) {
        ShopifyCodeEntity entity = shopifyCodeRepository.findByCode(code);

        if(entity != null) {    // 존재하는 경우
            return 0;
        } else {                // 존재하지 않는 경우
            return 1;
        }
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    /* 라이센스 관련 API - 임시방편 API로 곧 사용 안 할 예정 */
    /* -------------------------------------------------------------------------------------------------------------- */
    public List<ShopifyLicenseEntity> getLicenseListByEmail(String email) throws Exception {
        log.info(" @ Hello ShopifyService.getLicenseListByEmail ! ");
        List<ShopifyLicenseEntity> shopifyLicenseEntities = shopifyLicenseRepository.findByEmail(email);

        return shopifyLicenseEntities;
    }

    @Transactional
    public int insertShopifyLicense(ShopifyLicenseEntity shopifyLicenseEntity) throws Exception {
        log.info(" @ Hello ShopifyService.insertShopifyLicense ! ");
        ShopifyLicenseEntity result = shopifyLicenseRepository.save(shopifyLicenseEntity);

        return 1;
    }

    public int validateLicense(String licenseKey) throws Exception {
        log.info(" @ Hello ShopifyService.validateLicense ! ");
        ShopifyLicenseManagementEntity result = shopifyLicenseManagementRepository.findByLicenseKeyAndActive(licenseKey, 1);
        if(result == null)
            return 0;
        else
            return 1;
    }

    /* 특정 라이센스 키는 중복 등록 불가능하도록 변경  */
    public int checkUserLicense(ShopifyLicenseEntity shopifyLicenseEntity) throws Exception {
        log.info(" @ Hello ShopifyService.checkUserLicense ! ");

        List<ShopifyLicenseDuplicateProtectionEntity> entityList = shopifyLicenseDuplicateProtectionRepository.findAll();

        String userLicense = shopifyLicenseEntity.getLicenseKey();
        String userEmail = shopifyLicenseEntity.getEmail();

        for(ShopifyLicenseDuplicateProtectionEntity entity : entityList) {
            if(userLicense.equalsIgnoreCase( entity.getLicenseKey() ) ) {   // 중복 불가한 라이센스 검사
                if(shopifyLicenseRepository.findByEmailAndLicenseKey(userEmail, userLicense) != null) { // 사용자에게 동일 라이센스 존재하는지 검사
                    return 0;
                }
            }
        }
        return 1;
    }
}
