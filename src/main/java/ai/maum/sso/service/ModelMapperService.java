package ai.maum.sso.service;

import org.springframework.stereotype.Service;

import org.modelmapper.ModelMapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by kirk@mindslab.ai on 2021-01-11
 */
@Service
public class ModelMapperService {

    private final ModelMapper modelMapper;

    ModelMapperService()
    {
        this.modelMapper = new ModelMapper();
    }

    public <D, T> D map(final T entity, Class<D> outClass)
    {
        modelMapper.getConfiguration().setAmbiguityIgnored(false);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> D mapAmbiguityIgnored(final T entity, Class<D> outClass)
    {
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outClass)
    {
        return entityList.stream()
                .map(entity -> map(entity, outClass))
                .collect(Collectors.toList());
    }
}
