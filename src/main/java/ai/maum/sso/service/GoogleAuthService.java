package ai.maum.sso.service;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.common.Utils;
import ai.maum.sso.domain.*;
import ai.maum.sso.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.Connection;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.social.google.api.plus.Person;
import org.springframework.social.google.api.plus.PlusOperations;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class GoogleAuthService {

    private final UserTRepository userTRepository;
    private final BillingTRepository billingTRepository;
    private final OAuthClientRepository oAuthClientRepository;
    private final OAuthTokenRepository oAuthTokenRepository;
    private final ServicePerProductRepository servicePerProductRepository;
    private final SupportService supportService;
    private final RedisTemplate redisTemplate;

    private final String MAUM_AI___SIGNUP_PATH = "/member/signupForm";
    private final String SIGNUP_CALLBACK___PATH = "/hq/oauth/signup-member-callback";

    @Value("${my.domain}")
    private String DOMAIN;

    @Value("${google.client_id}")
    private String GOOGLE___CLIENT_ID;

    @Value("${google.secret}")
    private String GOOGLE___SECRET;

    @Value("${timeout.access_token}")
    private int TIMEOUT___ACCESS_TOKEN;

    @Value("${timeout.refresh_token}")
    private int TIMEOUT___REFRESH_TOKEN;

    @Value("${time.db-diff-time}")
    private int DB_DIFF_TIME;

    @Value("${url.maum-ai}")
    private String URL___MAUM_AI;

    /* 구글 인증을 위한 URL 생성 */
    GoogleConnectionFactory mGoogle = null;

    @PostConstruct
    public void init() {
        mGoogle = new GoogleConnectionFactory(
                GOOGLE___CLIENT_ID,
                GOOGLE___SECRET
        );
    }

    /*
    ** 서비스 서버로부터 로그인 요청에 대한 처리 로직
    */
    public String authorize(HttpServletResponse response, String stateMaumHQ, String response_type, String client_id, String redirect_uri, String state) {
        String returnUri = null;
        OAuthTokenEntity tokenEntity = null;

        /* 쿠키가 존재하면, 재로그인으로 바로 콜백 호출한다. */
        if(stateMaumHQ != null && (tokenEntity = oAuthTokenRepository.findByState(stateMaumHQ)) != null && Utils.isExpiredTime(tokenEntity.getRefreshExpireDatetime()) == false) {
            log.info("=====>> SsoService.authorize : 구글 재로그인!");
            return redirect_uri + "?state=" + state + "&code=" + tokenEntity.getAuthorizationCode();
        }

        /* 쿠키가 존재하면, 초기 로그인으로 구글 로그인 절차 수행 */
        {
            log.info("=====>> SsoService.authorize : 구글 초기 로그인!");

            // 존재하지 않을 시, 새로운 UUID 생성
            String maumHqState = UUID.randomUUID().toString();

            Cookie myCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, maumHqState);
            myCookie.setDomain("maum.ai");
            myCookie.setMaxAge(24 * 60 * 60);
            myCookie.setPath("/");
            response.addCookie(myCookie);

            returnUri = getGoogleAuthUrl(client_id, maumHqState);
        }
        return returnUri;
    }

    public String getGoogleAuthUrl(String clientId, String state) {
        OAuthTokenEntity entity = new OAuthTokenEntity();
        entity.setState(state);
        entity.setClientId(clientId);
        entity.setAuthorizationCode(UUID.randomUUID().toString());
        entity.setAccessToken(UUID.randomUUID().toString());
        entity.setRefreshToken(UUID.randomUUID().toString());

        LocalDateTime createDate = LocalDateTime.now();
        entity.setCreateDatetime(createDate);
        entity.setUpdateDatetime(createDate);
        oAuthTokenRepository.save(entity);


        /* 구글 인증을 위한 URL 생성 */
        OAuth2Parameters googleOAuth2Parameters = new OAuth2Parameters();
        googleOAuth2Parameters.setRedirectUri(DOMAIN + "/hq/oauth/callback");
        googleOAuth2Parameters.setScope("https://www.googleapis.com/auth/userinfo.email");
        googleOAuth2Parameters.setState(state);

        OAuth2Operations oauthOperations = mGoogle.getOAuthOperations();
        String url = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, googleOAuth2Parameters);

        return url;
    }

    /*
    ** 구글 로그인 콜백 처리
    */
    public String procGoogleCallback(String code, String state) {

        /* 구글 접근 토큰 조회 */
        OAuth2Operations oauthOperations = mGoogle.getOAuthOperations();
        AccessGrant accessGrant = null;
        try {
            log.info("@ SsoServer.progGoogleCallback : DOMAIN={}, code={}", DOMAIN, code);
            accessGrant = oauthOperations.exchangeForAccess(code, DOMAIN + "/hq/oauth/callback", null);
        }catch(HttpClientErrorException e) {
            log.info("HttpClientErrorException  : {}", e.getMessage());
            throw new UsernameNotFoundException("");
        }
        String accessToken = accessGrant.getAccessToken();
        Long expireTime = accessGrant.getExpireTime();
        log.info("accessToken  : {}", accessToken);
        log.info("expireTime  : {}", expireTime);

        if (expireTime != null && expireTime < System.currentTimeMillis()) {
            accessToken = accessGrant.getRefreshToken();
            log.info("accessToken is expired. refresh token = {}", accessToken);
        }

        /* 구글 프로파일 정보 조회 */
        Connection<Google> connection = mGoogle.createConnection(accessGrant);
        Google google = connection == null ? new GoogleTemplate(accessToken) : connection.getApi();

        PlusOperations plusOperations = google.plusOperations();
        Person person = plusOperations.getGoogleProfile();

        String email = person.getAccountEmail();
        if(email == null){
            for (Map.Entry<String, String> entry : person.getEmails().entrySet()) {
                if(entry.getValue().equalsIgnoreCase("account")) {
                    email=  entry.getKey();
                    log.info("email : " +email);
                }
            }
        }
        log.info("@ oauth.callback : person={}", person.toString());


        /* 토큰 발행을 위한 초기값 설정 */
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if(oAuthTokenEntity != null) {
            oAuthTokenEntity.setAuthorizationCode(UUID.randomUUID().toString());
            oAuthTokenEntity.setEmail(email);
            LocalDateTime updateDate = LocalDateTime.now();
            oAuthTokenEntity.setUpdateDatetime(updateDate);
            oAuthTokenRepository.save(oAuthTokenEntity);
        }
        else {
            throw new RuntimeException("state not exist:state=" + state);
        }

        /* maum.ai 서비스가 활성화 되어 있지 않으면 회원 등록 수행 */
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        log.info("# maum.ai member(구글)'s status = {} / {}", email, (userTEntity == null ? null : userTEntity.toString()));

        if( (oAuthClientEntity != null && oAuthClientEntity.getFlagCreateMember() == 1)
            && (userTEntity == null
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___INIT
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___UNSUBSCRIBED
                || userTEntity.getPrivacyAgree() == 0) ) {
            String callback_uri = String.format("%s%s?state=%s", DOMAIN, SIGNUP_CALLBACK___PATH, state);
            return String.format("%s%s?email=%s&state=%s&callback_uri=%s", URL___MAUM_AI, MAUM_AI___SIGNUP_PATH, email, state, callback_uri);
        }
        else {
            /* 이미 등록된 고객이면, 서비스 콜백으로 리다이렉트 한다. */
            return returnServiceCallback(state);
        }
    }

    /*
    ** 서비스 콜백으로 접근 코드 반환
    */
    public String returnServiceCallback(String state) {
        /* 토큰 정보 조회 */
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if(oAuthTokenEntity == null) {
            log.error("# procGoogleCallback@oauthMapper.getTokenByState({}) fail", state);
        }

        /* RedirectUri 조회 */
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        if(oAuthClientEntity == null) {
            log.error("# procGoogleCallback@oauthMapper.getClientInfo({}) fail", oAuthClientEntity.getClientId());
        }
        return oAuthClientEntity.getCallbackUri() + "?state=" + state + "&code=" + oAuthTokenEntity.getAuthorizationCode();
    }

    /* ------------------------------------------------------------------------------------------------------------- */
    /* 통합로그인 2.0 - 추후 1.0 제거 예정  */

    public String ssoAuthorize(HttpServletResponse response, String stateMaumHQ, String response_type, String client_id, String redirect_uri, String state) {
        String returnUri = null;
        OAuthTokenEntity tokenEntity = null;

        /* 쿠키가 존재하면, 초기 로그인으로 구글 로그인 절차 수행 */
        log.info("=====>> SsoService.ssoAuthorize : 구글 로그인!");

        // 존재하지 않을 시, 새로운 UUID 생성
        String maumHqState = UUID.randomUUID().toString();

        Cookie myCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, maumHqState);
        myCookie.setDomain("maum.ai");
        myCookie.setMaxAge(24 * 60 * 60);
        myCookie.setPath("/");
        response.addCookie(myCookie);

        returnUri = getSsoGoogleAuthUrl(client_id, maumHqState);

        // State : RedirectUri 조합으로 레디스에 저장
        getSsoStateForGoogleAuthUrl(returnUri, redirect_uri);

        return returnUri;
    }

    public String getSsoGoogleAuthUrl(String clientId, String state) {
        OAuthTokenEntity entity = new OAuthTokenEntity();
        entity.setState(state);
        entity.setClientId(clientId);
        entity.setAuthorizationCode(UUID.randomUUID().toString());
        entity.setAccessToken(UUID.randomUUID().toString());
        entity.setRefreshToken(UUID.randomUUID().toString());

        LocalDateTime createDate = LocalDateTime.now();
        entity.setCreateDatetime(createDate);
        entity.setUpdateDatetime(createDate);
        oAuthTokenRepository.save(entity);


        /* 구글 인증을 위한 URL 생성 */
        OAuth2Parameters googleOAuth2Parameters = new OAuth2Parameters();
        googleOAuth2Parameters.setRedirectUri(DOMAIN + "/sso/oauth/token");
        googleOAuth2Parameters.setScope("https://www.googleapis.com/auth/userinfo.email");
        googleOAuth2Parameters.setState(state);

        OAuth2Operations oauthOperations = mGoogle.getOAuthOperations();
        String url = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, googleOAuth2Parameters);

        return url;
    }

    private void getSsoStateForGoogleAuthUrl(String returnUri, String redirect_uri) {
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(returnUri).build();
        MultiValueMap<String,String> multiValueMap = uriComponents.getQueryParams();
        String googleState = multiValueMap.getFirst("state");
        redisTemplate.opsForValue().set(googleState, redirect_uri);

        log.info(String.format("=====>> Store state(%s) key in redis when authorize(google) call : %s", googleState, redisTemplate.opsForValue().get(googleState).toString()));
    }

    /*
     ** 구글 로그인 콜백 처리
     */
    public String procSsoGoogleCallback(String code, String state) {

        /* 구글 접근 토큰 조회 */
        OAuth2Operations oauthOperations = mGoogle.getOAuthOperations();
        AccessGrant accessGrant = null;
        try {
            log.info("@ SsoServer.progGoogleCallback : DOMAIN={}, code={}", DOMAIN, code);
            accessGrant = oauthOperations.exchangeForAccess(code, DOMAIN + "/sso/oauth/token", null);
        }catch(HttpClientErrorException e) {
            log.info("HttpClientErrorException  : {}", e.getMessage());
            throw new UsernameNotFoundException("");
        }
        String accessToken = accessGrant.getAccessToken();
        Long expireTime = accessGrant.getExpireTime();
        log.info("accessToken  : {}", accessToken);
        log.info("expireTime  : {}", expireTime);

        if (expireTime != null && expireTime < System.currentTimeMillis()) {
            accessToken = accessGrant.getRefreshToken();
            log.info("accessToken is expired. refresh token = {}", accessToken);
        }

        /* 구글 프로파일 정보 조회 */
        Connection<Google> connection = mGoogle.createConnection(accessGrant);
        Google google = connection == null ? new GoogleTemplate(accessToken) : connection.getApi();

        PlusOperations plusOperations = google.plusOperations();
        Person person = plusOperations.getGoogleProfile();

        String email = person.getAccountEmail();
        if(email == null){
            for (Map.Entry<String, String> entry : person.getEmails().entrySet()) {
                if(entry.getValue().equalsIgnoreCase("account")) {
                    email=  entry.getKey();
                    log.info("email : " +email);
                }
            }
        }
        log.info("@ oauth.SsoCallback : person={}", person.toString());


        /* 토큰 발행을 위한 초기값 설정 */
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if(oAuthTokenEntity != null) {
            oAuthTokenEntity.setAuthorizationCode(UUID.randomUUID().toString());
            oAuthTokenEntity.setEmail(email);
            LocalDateTime updateDate = LocalDateTime.now();
            oAuthTokenEntity.setUpdateDatetime(updateDate);
            oAuthTokenRepository.save(oAuthTokenEntity);
        }
        else {
            throw new RuntimeException("state not exist:state=" + state);
        }

        /* maum.ai 서비스가 활성화 되어 있지 않으면 회원 등록 수행 */
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        log.info("# maum.ai member(구글)'s status = {} / {}", email, (userTEntity == null ? null : userTEntity.toString()));

        if( (oAuthClientEntity != null && oAuthClientEntity.getFlagCreateMember() == 1)
                && (userTEntity == null
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___INIT
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___UNSUBSCRIBED
                || userTEntity.getPrivacyAgree() == 0) ) {
            String callback_uri = String.format("%s%s?state=%s", DOMAIN, SIGNUP_CALLBACK___PATH, state);
            return String.format("%s%s?email=%s&state=%s&callback_uri=%s", URL___MAUM_AI, MAUM_AI___SIGNUP_PATH, email, state, callback_uri);
        }
        else {
            /* 이미 등록된 고객이면, 서비스 콜백으로 리다이렉트 한다. */
            return returnSsoServiceCallback(state);
        }
    }

    /*
     ** 서비스 콜백으로 접근 코드 반환
     */
    public String returnSsoServiceCallback(String state) {
        String redirect_uri = Optional.ofNullable(redisTemplate.opsForValue().get(state))
                .orElseThrow(() -> new RuntimeException(String.format("Not found state(%s) in redis", state))).toString();

        log.info(String.format("=====>> returnServiceCallback about state(%s) : %s", state, redirect_uri));

        return redirect_uri;
    }

    /* ------------------------------------------------------------------------------------------------------------- */



    /*
    ** 신규 인증토큰 발행 요청
    */
    public HashMap<String, Object> requestPublishingTokens(String grant_type, String request_uri, String code, String refresh_token) {
        final String GRANT_TYPE___CODE = "authorization_code";
        final String GRANT_TYPE___REFRESH = "refresh_token";

        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        OAuthTokenEntity oAuthTokenEntity = null;

        /* 조회 */
        if(grant_type.equals(GRANT_TYPE___CODE)) {
            oAuthTokenEntity = oAuthTokenRepository.findByAuthorizationCode(code);
        }
        else if(grant_type.equals(GRANT_TYPE___REFRESH)) {
            oAuthTokenEntity = oAuthTokenRepository.findByRefreshToken(refresh_token);
        }
        else {
            log.error("# ERROR >> Invalid grant_type >> grant_type={}", grant_type);
            return null;
        }

        if(oAuthTokenEntity == null) {
            log.error("# ERROR >> requestPublishingTokens @ oauthMapper.getTokenByXXX({} or {}) fail >> ", code, refresh_token);
            return null; // 등록되지 않은 코드
        }

        /* 토큰 발행 */
        OAuthTokenEntity token = publishTokens(oAuthTokenEntity.getAuthorizationCode());
        if(token == null) {
            log.error("# ERROR >> requestPublishingTokens @ publishTokens({}) fail >> ");
            return null; // 등록 실패
        }

        UserTEntity user = userTRepository.findByEmail(oAuthTokenEntity.getEmail());

        resultMap.put("email", oAuthTokenEntity.getEmail());
        resultMap.put("name", (user == null ? "" : user.getName()));
        resultMap.put("phone", (user == null ? "" : user.getPhone()));
        resultMap.put("access_token", token.getAccessToken());
        resultMap.put("access_expire_time", token.getAccessExpireDatetime().toString());
        resultMap.put("refresh_token", token.getRefreshToken());
        resultMap.put("refresh_expire_time", token.getRefreshExpireDatetime().toString());

        try {
            supportService.recordLoginHistory(oAuthTokenEntity.getEmail());
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /*
    ** 사용자의 api 정보 요청 - 2020. 11. 27 - LYJ
    */
    public HashMap<String, Object> apiInfo(HashMap<String, Object> resultMap) {

        String userEmail = resultMap.get("email").toString();

        UserTEntity entity = userTRepository.findByEmail(userEmail);

        resultMap.put("apiId", entity.getApiId());
        resultMap.put("apiKey", entity.getApiKey());

        return resultMap;
    }

    /*
    ** [주의] 클라이언트에서 로그아웃 시에, 반드시 쿠키 삭제해야 함.
    */
    public void cleanTokens(String clientId, String accessToken) {
        /* 조회 */
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByAccessToken(accessToken);
        if(oAuthTokenEntity == null) {
            log.error("# cleanTokens@oAuthTokenRepository.findByAccessToken( {} ) fail", accessToken);
            return;
        }

        /* 조회 */
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(clientId);
        if(oAuthClientEntity == null) {
            log.error("# cleanTokens@oAuthClientRepository.findByClientId( {} ) fail", clientId);
            return;
        }

        /* 현재 사이트의 토큰 삭제 */
        oAuthTokenRepository.delete(oAuthTokenEntity);

        /* 남아있는 토큰 목록 조회 */
        List<OAuthTokenEntity> tokenList = oAuthTokenRepository.findAllByEmail(oAuthTokenEntity.getEmail());
        if(tokenList != null && tokenList.size() > 0) {
            // logout 방식 변경으로 불필요함.
            //SSOInterface.cleanToken(clientInfo.getLogout_noti_uri(), clientId, authInfo.getEmail(), authInfo.getAccess_token());
        }

        /* 나머지 모든 사이트의 토큰 삭제 */
        String email = oAuthTokenEntity.getEmail();
        oAuthTokenRepository.deleteAllByEmail(email);
    }

    private OAuthTokenEntity publishTokens(String code) {
        OAuthTokenEntity entity = oAuthTokenRepository.findByAuthorizationCode(code);

        if(entity == null)
            return null;

        LocalDateTime now = LocalDateTime.now();
        entity.setAccessToken(UUID.randomUUID().toString());
        entity.setAccessExpireDatetime(now.plusMinutes(TIMEOUT___ACCESS_TOKEN));
        entity.setRefreshToken(UUID.randomUUID().toString());
        entity.setRefreshExpireDatetime(now.plusMinutes(TIMEOUT___REFRESH_TOKEN));

        oAuthTokenRepository.save(entity);
        return entity;
    }


    /* 사용자의 이메일로 결제한 Product 조회 */
    public int getUserProduct(String email) {

        int product = 0;

        UserTEntity userTEntity = userTRepository.findByEmail(email);
        if(userTEntity != null) {
            int userNo = userTEntity.getUserNo();

            BillingTEntity billingTEntity = billingTRepository.findByUserNo(userNo);
            if (billingTEntity != null) {
                product = billingTEntity.getProduct();
            } else if (userTEntity.getProduct() != null) {
                product = userTEntity.getProduct();
            } else {
                product = ConstDef.PLAN_BUSINESS;
            }
        }
        return product;
    }

    /* 고객의 결제내역에 따른 사용 가능 서비스 목록 조회 */
    public HashMap<String, Object> getUsableServiceList(int product, String email) {

        HashMap<String, Object> resultMap = new HashMap<>();

        ServicePerProductEntity service = servicePerProductRepository.findByProduct(product);

        resultMap.put("product_name", service.getName());
        resultMap.put("cloud_api", service.getCloud_api());
        resultMap.put("ai_builder", service.getAi_builder());
        resultMap.put("minutes", service.getMinutes());
        resultMap.put("fast_ai", service.getFast_ai());
        resultMap.put("ava", service.getAva());
        resultMap.put("data", service.getData());
        resultMap.put("academy", service.getAcademy());

        // 무료 사용자 여부 파악
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        if(userTEntity != null) {
            int userStatus = userTEntity.getStatus();
            if ((userStatus == ConstDef.USER_SUBSCRIBE_STATUS_INIT && userTEntity.getPrivacyAgree() != 1) || userStatus == ConstDef.USER_SUBSCRIBE_STATUS_UNSUBSCRIBED)
                resultMap.put("status", "free");
            else if(userStatus == ConstDef.USER_SUBSCRIBE_STATUS_FREE || (userStatus == ConstDef.USER_SUBSCRIBE_STATUS_INIT && userTEntity.getPrivacyAgree() == 1))
                resultMap.put("status", "payment");     // 개인 정보 동의를 한 회원의 경우 무료 사용 기간을 보내고 있는 고객
            else
                resultMap.put("status", "payment");
        }
        return resultMap;
    }
}
