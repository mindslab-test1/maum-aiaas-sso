package ai.maum.sso.service;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.common.Utils;
import ai.maum.sso.domain.OAuthClientEntity;
import ai.maum.sso.domain.OAuthTokenEntity;
import ai.maum.sso.domain.UserTEntity;
import ai.maum.sso.repository.OAuthClientRepository;
import ai.maum.sso.repository.OAuthTokenRepository;
import ai.maum.sso.repository.UserTRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.UserOperations;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class FacebookAuth_Service {

    private final UserTRepository userTRepository;
    private final OAuthTokenRepository oAuthTokenRepository;
    private final OAuthClientRepository oAuthClientRepository;
    private final RedisTemplate redisTemplate;

    private final String MAUM_AI___SIGNUP_PATH = "/member/signupForm";
    private final String SIGNUP_CALLBACK___PATH = "/hq/oauth/signup-member-callback";

    @Value("${my.domain}")
    private String DOMAIN;

    @Value("${facebook.appId}")
    private String FACEBOOK___APP_ID;

    @Value("${facebook.secretCode}")
    private String FACEBOOK___SECRET_CODE;

    @Value("${timeout.access_token}")
    private int TIMEOUT___ACCESS_TOKEN;

    @Value("${timeout.refresh_token}")
    private int TIMEOUT___REFRESH_TOKEN;

    @Value("${time.db-diff-time}")
    private int DB_DIFF_TIME;

    @Value("${url.maum-ai}")
    private String URL___MAUM_AI;

    FacebookConnectionFactory mFacebook = null;

    @PostConstruct
    public void init() {
        mFacebook = new FacebookConnectionFactory(
                FACEBOOK___APP_ID, FACEBOOK___SECRET_CODE
        );
    }

    /* 로그인 요청에 대한 처리 */
    public String authorize(HttpServletResponse response, String codeMaumHQ, String response_type, String client_id, String redirect_uri, String state) {
        String returnUrl = null;
        OAuthTokenEntity tokenEntity = null;


        /* 쿠키가 존재하면, 재로그인으로 바로 콜백 호출한다. */
        if(codeMaumHQ != null && (tokenEntity = oAuthTokenRepository.findByState(codeMaumHQ)) != null && Utils.isExpiredTime(tokenEntity.getRefreshExpireDatetime()) == false) {
            log.info("=====>> SsoService.authorize : 페이스북 재로그인!");
            return redirect_uri + "?state=" + state + "&code=" + tokenEntity.getAuthorizationCode();
        }

        /* 쿠키가 존재하면, 초기 로그인으로 페이스북 로그인 절차 수행 */
        {
            log.info("=====>> SsoService.authorize : 페이스북 초기 로그인!");

            Cookie myCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, state);
            myCookie.setDomain("maum.ai");
            myCookie.setMaxAge(24 * 60 * 60);
            myCookie.setPath("/");
            response.addCookie(myCookie);

            returnUrl = getFacebookAuthUrl(client_id, state, redirect_uri);
        }
        return returnUrl;
    }



    public String getFacebookAuthUrl(String clientId, String state, String redirectUri) {

        /* 신규 토큰 생성 및 등록 */
        OAuthTokenEntity entity = new OAuthTokenEntity();
        entity.setState(state);
        entity.setClientId(clientId);
        entity.setAuthorizationCode(UUID.randomUUID().toString());
        entity.setAccessToken(UUID.randomUUID().toString());
        entity.setRefreshToken(UUID.randomUUID().toString());

        LocalDateTime createDate = LocalDateTime.now();
        entity.setCreateDatetime(createDate);
        entity.setUpdateDatetime(createDate);
        oAuthTokenRepository.save(entity);

        /*  인증을 위한 URL 생성 */
        OAuth2Parameters facebookOAuth2Parameters = new OAuth2Parameters();
        facebookOAuth2Parameters.setRedirectUri(DOMAIN + "/login/facebook/callback");
        facebookOAuth2Parameters.setScope("email");
        facebookOAuth2Parameters.setState(state);

        OAuth2Operations oauthOperations = mFacebook.getOAuthOperations();
        String facebook_url = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, facebookOAuth2Parameters);

        return facebook_url;
    }


    /* 페이스북 로그인 콜백 처리 */
    public String procFacebookCallback(String code, String state) {

        // 페이스북 접근 토큰 조회
        OAuth2Operations oauthOperations = mFacebook.getOAuthOperations();
        AccessGrant accessGrant = null;
        try {

            log.info("@ SsoServer.procFacebookCallback : DOMAIN={}, code={}", DOMAIN, code);
            accessGrant = oauthOperations.exchangeForAccess(code, DOMAIN + "/login/facebook/callback", null);

        } catch (HttpClientErrorException e) {

            log.info("HttpClientErrorException  : {}", e.getMessage());
            throw new UsernameNotFoundException("");

        }
        
        String accessToken = accessGrant.getAccessToken();
        Long expireTime = accessGrant.getExpireTime();
        log.info("accessToken  : {}, expireTime  : {}", accessToken, expireTime);

        // refresh token 발급 요청
        if (expireTime != null && expireTime < System.currentTimeMillis()) {
            accessToken = accessGrant.getRefreshToken();
            log.info("accessToken is expired. refresh token = {}", accessToken);
        }

        // facebook 프로파일 정보 조회
        Connection<Facebook> connection = mFacebook.createConnection(accessGrant);
        Facebook facebook = connection == null ? new FacebookTemplate(accessToken) : connection.getApi();
        UserOperations userOperations = facebook.userOperations();

        String[] fields = {"id", "email", "name"};
        User userProfile = facebook.fetchObject("me", User.class, fields);

        String email = userProfile.getEmail();
        // 토큰 발행을 위한 초기값 설정
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if(oAuthTokenEntity != null) {
            oAuthTokenEntity.setAuthorizationCode(UUID.randomUUID().toString());
            oAuthTokenEntity.setEmail(email);
            LocalDateTime updateDate = LocalDateTime.now();
            oAuthTokenEntity.setUpdateDatetime(updateDate);
            oAuthTokenRepository.save(oAuthTokenEntity);
        }
        else {
            throw new RuntimeException("state not exist:state=" + state);
        }

        /* maum.ai 서비스가 활성화 되어 있지 않으면 회원 등록 수행 */
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        log.info("# maum.ai member(페이스북)'s status = {} / {}", email, (userTEntity == null ? null : userTEntity.toString()));

        if( (oAuthClientEntity != null && oAuthClientEntity.getFlagCreateMember() == 1)
                && (userTEntity == null
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___INIT
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___UNSUBSCRIBED
                || userTEntity.getPrivacyAgree() == 0) ) {
            String callback_uri = String.format("%s%s?state=%s", DOMAIN, SIGNUP_CALLBACK___PATH, state);
            return String.format("%s%s?email=%s&state=%s&callback_uri=%s", URL___MAUM_AI, MAUM_AI___SIGNUP_PATH, email, state, callback_uri);
        }
        else {
            /* 이미 등록된 고객이면, 서비스 콜백으로 리다이렉트 한다. */
            return returnFacebookServiceCallback(state);
        }
    }

    /* 서비스 콜백으로 접근 코드 반환 */
    public String returnFacebookServiceCallback(String state) {

        // 토큰 정보 조회
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if(oAuthTokenEntity == null) {
            log.error("# procFacebookCallback@oauthMapper.getTokenByState({}) fail", state);
        }

        // RedirectUri 조회
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        if(oAuthTokenEntity == null) {
            log.error("# procFacebookCallback@oauthMapper.getClientInfo({}) fail", oAuthTokenEntity.getClientId());
        }
        return oAuthClientEntity.getCallbackUri() + "?state=" + state + "&code=" + oAuthTokenEntity.getAuthorizationCode();
    }


    /* ------------------------------------------------------------------------------------------------------------- */
    /* 통합로그인 2.0 - 추후 1.0 제거 예정  */

    public String ssoAuthorize(HttpServletResponse response, String stateMaumHQ, String response_type, String client_id, String redirect_uri, String state) {
        String returnUri = null;
        OAuthTokenEntity tokenEntity = null;

        /* 쿠키가 존재하면, 초기 로그인으로 구글 로그인 절차 수행 */
        log.info("=====>> SsoService.ssoAuthorize : 페이스북 로그인!");

        // 존재하지 않을 시, 새로운 UUID 생성
        String maumHqState = UUID.randomUUID().toString();

        Cookie myCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, maumHqState);
        myCookie.setDomain("maum.ai");
        myCookie.setMaxAge(24 * 60 * 60);
        myCookie.setPath("/");
        response.addCookie(myCookie);

        returnUri = getSsoFacebookAuthUrl(client_id, maumHqState);

        // State : RedirectUri 조합으로 레디스에 저장
        getSsoStateForFacebookAuthUrl(returnUri, redirect_uri);

        return returnUri;
    }

    public String getSsoFacebookAuthUrl(String clientId, String state) {

        /* 신규 토큰 생성 및 등록 */
        OAuthTokenEntity entity = new OAuthTokenEntity();
        entity.setState(state);
        entity.setClientId(clientId);
        entity.setAuthorizationCode(UUID.randomUUID().toString());
        entity.setAccessToken(UUID.randomUUID().toString());
        entity.setRefreshToken(UUID.randomUUID().toString());

        LocalDateTime createDate = LocalDateTime.now();
        entity.setCreateDatetime(createDate);
        entity.setUpdateDatetime(createDate);
        oAuthTokenRepository.save(entity);

        /*  인증을 위한 URL 생성 */
        OAuth2Parameters facebookOAuth2Parameters = new OAuth2Parameters();
        facebookOAuth2Parameters.setRedirectUri(DOMAIN + "/facebook/oauth/token");
        facebookOAuth2Parameters.setScope("email");
        facebookOAuth2Parameters.setState(state);

        OAuth2Operations oauthOperations = mFacebook.getOAuthOperations();
        String facebook_url = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, facebookOAuth2Parameters);

        return facebook_url;
    }

    private void getSsoStateForFacebookAuthUrl(String returnUri, String redirect_uri) {
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(returnUri).build();
        MultiValueMap<String,String> multiValueMap = uriComponents.getQueryParams();
        String facebookState = multiValueMap.getFirst("state");
        redisTemplate.opsForValue().set(facebookState, redirect_uri);

        log.info(String.format("=====>> Store state(%s) key in redis when authorize(facebook) call : %s", facebookState, redisTemplate.opsForValue().get(facebookState).toString()));
    }

    /*
     ** 페이스북 로그인 콜백 처리
     */
    public String procSsoFacebookCallback(String code, String state) {

        // 페이스북 접근 토큰 조회
        OAuth2Operations oauthOperations = mFacebook.getOAuthOperations();
        AccessGrant accessGrant = null;
        try {

            log.info("@ SsoServer.progFacebookCallback : DOMAIN={}, code={}", DOMAIN, code);
            accessGrant = oauthOperations.exchangeForAccess(code, DOMAIN + "/facebook/oauth/token", null);

        }catch(HttpClientErrorException e) {

            log.info("HttpClientErrorException  : {}", e.getMessage());
            throw new UsernameNotFoundException("");
        }

        String accessToken = accessGrant.getAccessToken();
        Long expireTime = accessGrant.getExpireTime();

        log.info("accessToken  : {}, expireTime  : {}", accessToken, expireTime);

        // refresh token 발급 요청
        if (expireTime != null && expireTime < System.currentTimeMillis()) {
            accessToken = accessGrant.getRefreshToken();
            log.info("accessToken is expired. refresh token = {}", accessToken);
        }

        // facebook 프로파일 정보 조회
        Connection<Facebook> connection = mFacebook.createConnection(accessGrant);
        Facebook facebook = connection == null ? new FacebookTemplate(accessToken) : connection.getApi();
        UserOperations userOperations = facebook.userOperations();

        String[] fields = {"id", "email", "name"};
        User userProfile = facebook.fetchObject("me", User.class, fields);

        String email = userProfile.getEmail();

        /* 토큰 발행을 위한 초기값 설정 */
        OAuthTokenEntity oAuthTokenEntity = oAuthTokenRepository.findByState(state);
        if(oAuthTokenEntity != null) {
            oAuthTokenEntity.setAuthorizationCode(UUID.randomUUID().toString());
            oAuthTokenEntity.setEmail(email);
            LocalDateTime updateDate = LocalDateTime.now();
            oAuthTokenEntity.setUpdateDatetime(updateDate);
            oAuthTokenRepository.save(oAuthTokenEntity);
        }
        else {
            throw new RuntimeException("state not exist:state=" + state);
        }

        /* maum.ai 서비스가 활성화 되어 있지 않으면 회원 등록 수행 */
        UserTEntity userTEntity = userTRepository.findByEmail(email);
        OAuthClientEntity oAuthClientEntity = oAuthClientRepository.findByClientId(oAuthTokenEntity.getClientId());
        log.info("# maum.ai member(페이스북)'s status = {} / {}", email, (userTEntity == null ? null : userTEntity.toString()));

        if( (oAuthClientEntity != null && oAuthClientEntity.getFlagCreateMember() == 1)
                && (userTEntity == null
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___INIT
//                || userTEntity.getStatus() == ConstDef.MAUM_AI_STATE___UNSUBSCRIBED
                || userTEntity.getPrivacyAgree() == 0) ) {
            String callback_uri = String.format("%s%s?state=%s", DOMAIN, SIGNUP_CALLBACK___PATH, state);
            return String.format("%s%s?email=%s&state=%s&callback_uri=%s", URL___MAUM_AI, MAUM_AI___SIGNUP_PATH, email, state, callback_uri);
        }
        else {
            /* 이미 등록된 고객이면, 서비스 콜백으로 리다이렉트 한다. */
            return returnSsoServiceCallback(state);
        }
    }

    /*
     ** 서비스 콜백으로 접근 코드 반환
     */
    public String returnSsoServiceCallback(String state) {
        String redirect_uri = Optional.ofNullable(redisTemplate.opsForValue().get(state))
                .orElseThrow(() -> new RuntimeException(String.format("Not found state(%s) in redis", state))).toString();

        log.info(String.format("=====>> returnServiceCallback about state(%s) : %s", state, redirect_uri));

        return redirect_uri;
    }

    /* ------------------------------------------------------------------------------------------------------------- */

    
    /* 신규 인증토큰 발행 요청 */
    public HashMap<String, Object> requestPublishingFacebookTokens(String grant_type, String request_uri, String code, String refresh_token) {
        final String GRANT_TYPE___CODE = "authorization_code";
        final String GRANT_TYPE___REFRESH = "refresh_token";

        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        OAuthTokenEntity oAuthTokenEntity = null;

        // 조회
        if(grant_type.equals(GRANT_TYPE___CODE)) {
            oAuthTokenEntity = oAuthTokenRepository.findByAuthorizationCode(code);
        }
        else if(grant_type.equals(GRANT_TYPE___REFRESH)) {
            oAuthTokenEntity = oAuthTokenRepository.findByRefreshToken(refresh_token);
        }
        else {
            log.error("# ERROR >> Invalid grant_type >> grant_type={}", grant_type);
            return null;
        }

        if(oAuthTokenEntity == null) {
            log.error("# ERROR >> requestPublishingTokens @ oauthMapper.getTokenByXXX({} or {}) fail >> ", code, refresh_token);
            return null; // 등록되지 않은 코드
        }

        // 토큰 발행
        OAuthTokenEntity token = publishFacebookTokens(oAuthTokenEntity.getAuthorizationCode());
        if(token == null) {      // 등록 실패
            log.error("# ERROR >> requestPublishingFacebookTokens @ publishTokens({}) fail >> ");
            return null;
        }

        UserTEntity user = userTRepository.findByEmail(oAuthTokenEntity.getEmail());

        resultMap.put("email", oAuthTokenEntity.getEmail());
        resultMap.put("name", (user == null ? "" : user.getName()));
        resultMap.put("phone", (user == null ? "" : user.getPhone()));
        resultMap.put("access_token", token.getAccessToken());
        resultMap.put("access_expire_time", token.getAccessExpireDatetime().toString());
        resultMap.put("refresh_token", token.getRefreshToken());
        resultMap.put("refresh_expire_time", token.getRefreshExpireDatetime().toString());

        return resultMap;
    }

    /* Token 발행 */
    private OAuthTokenEntity publishFacebookTokens(String code) {
        OAuthTokenEntity entity = oAuthTokenRepository.findByAuthorizationCode(code);

        if(entity == null)
            return null;

        LocalDateTime now = LocalDateTime.now();
        entity.setAccessToken(UUID.randomUUID().toString());
        entity.setAccessExpireDatetime(now.plusMinutes(TIMEOUT___ACCESS_TOKEN));
        entity.setRefreshToken(UUID.randomUUID().toString());
        entity.setRefreshExpireDatetime(now.plusMinutes(TIMEOUT___REFRESH_TOKEN));

        oAuthTokenRepository.save(entity);
        return entity;
    }
}
