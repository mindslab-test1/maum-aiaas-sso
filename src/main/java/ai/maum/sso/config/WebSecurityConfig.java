package ai.maum.sso.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@Order(1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/hq/oauth/**", "/hq/hello").permitAll()
                    .and()
                .authorizeRequests()
                    .antMatchers("/sso/**").permitAll()
                    .and()
                .authorizeRequests()
                    .antMatchers("/maum/**").permitAll()
                    .and()
                .authorizeRequests()
                    .antMatchers("/login/**").permitAll()
                    .and()
                .authorizeRequests()
                    .antMatchers("/shopify/**").permitAll()
                    .and()
                .authorizeRequests()
                    .antMatchers("/css/**").permitAll()
                    .and()
                .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                .csrf()
                    .disable()
                .logout()
                    .permitAll();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/css/**")
                .antMatchers("/js/**")
                .antMatchers("/font/**")
                .antMatchers("/images/**")
                .antMatchers("/favicon**")
                .antMatchers("/**")
        ;
    }
}
