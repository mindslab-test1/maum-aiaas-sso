package ai.maum.sso.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class JwtProperty {

    @Value("${app.jwt.secret}")
    private String secret;

    @Value("${app.jwt.access-token-expired-hours:1}")
    private long accessTokenExpiredHours;

    @Value("${app.jwt.refresh-token-expired-days:365}")
    private long refreshTokenExpiredDays;
}
