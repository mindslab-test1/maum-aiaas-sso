package ai.maum.sso.common.data;

public interface CommonMsg {

    public static final int ERR_CODE_SUCCESS = 200;
    public static final String ERR_MSG_SUCCESS = "SUCCESS";

    public static final int ERR_CODE_DUPLICATE_LICENSE = 300;
    public static final String ERR_MSG_DUPLICATE_LICENSE = "ALREADY REGISTERED";

    public static final int ERR_CODE_NOT_FOUND = 404;
    public static final String ERR_MSG_NOT_FOUND = "NOT FOUND";

    public static final int ERR_CODE_FAILURE = 500;
    public static final String ERR_MSG_FAILURE = "FAILURE";

    public static final int ERR_CODE_INVALID_PARAMS = 502;
    public static final String ERR_MSG_INVALID_PARAMS = "INVALID PARAMETERS";

    public static final int ERR_CODE_INVALID_LICENSE = 503;
    public static final String ERR_MSG_INVALID_LICENSE = "INVALID LICENSE KEY";

    public static final int ERR_CODE_TIMEOUT = 504;
    public static final String ERR_MSG_TIMEOUT = "TIMEOUT";

    public static final int ERR_CODE_NO_MATCHING_DATA = 506;
    public static final String ERR_MSG_NO_MATCHING_DATA = "NO MATCHING DATA";

    public static final int ERR_CODE_EMAIL = 512;
    public static final String ERR_MSG_EMAIL = "이메일을 확인해주세요.";

    public static final int ERR_CODE_CERT_CODE = 513;
    public static final String ERR_MSG_CERT_CODE = "이메일 인증을 진행해주세요.";
}
