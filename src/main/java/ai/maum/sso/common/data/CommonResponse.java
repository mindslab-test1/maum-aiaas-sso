package ai.maum.sso.common.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;

@Data
//@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommonResponse<T> implements Serializable {
    private int code;
    private String msg;
    private T data;

    public CommonResponse() {
        this.code = CommonMsg.ERR_CODE_SUCCESS;
        this.msg = CommonMsg.ERR_MSG_SUCCESS;
    }

    public CommonResponse(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
