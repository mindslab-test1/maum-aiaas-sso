package ai.maum.sso.common;

import org.springframework.beans.factory.annotation.Value;

public class ConstDef {

    public static final String COOKIE_NAME___MAUM_HQ___STATE = "MaumHQ_State";
    public static final String COOKIE_NAME___MAIN_JSESSION = "MAIN_JSESSIONID";
    public static final String COOKIE_NAME___AIBUILDER_JSESSION = "AIBUILDER_JSESSIONID";
    public static final String MAUM_AID = "MAUM_AID";
    public static final String MAUM_RID = "MAUM_RID";

    public static final int MAUM_AI_STATE___INIT = 0;
    public static final int MAUM_AI_STATE___FREE = 1;
    public static final int MAUM_AI_STATE___SUBSCRIBED = 2;
    public static final int MAUM_AI_STATE___UNSUBSCRIBING = 3;
    public static final int MAUM_AI_STATE___UNSUBSCRIBED = 4;

    public static final String AUTH_TYPE_SIGN = "TYPE_SIGHUP";
    public static final String AUTH_TYPE_PASSWORD = "TYPE_PASSWORD";

    public static final String AUTH_STATUS_READY = "AT0001";
    public static final String AUTH_STATUS_EXPIRE = "AT0002";
    public static final String AUTH_STATUS_COMPLETE = "AT0003";

    public static final String ACCOUNT_STATUS_READY = "ST0001";
    public static final String ACCOUNT_STATUS_ACTIVATE = "ST0002";
    public static final String ACCOUNT_STATUS_INACTIVATE = "ST0003";

    public final static int USER_SUBSCRIBE_STATUS_INIT = 0;             // 로그인만 한 고객
    public final static int USER_SUBSCRIBE_STATUS_FREE = 1;             // 초기 카드 등록 고객
    public final static int USER_SUBSCRIBE_STATUS_SUBSCRIBED = 2;       // 정기 결제 고객
    public final static int USER_SUBSCRIBE_STATUS_UNSUBSCRIBING = 3;    // 정기 결제 고객 중 해지 신청 고객
    public final static int USER_SUBSCRIBE_STATUS_UNSUBSCRIBED = 4;     // 구독 취소 고객

    public final static int PLAN_BASIC = 2;
    public final static int PLAN_BUSINESS = 3;

    public final static String STORE_PRODUCT_BUY = "BUY";                        // 스토어에서 고객이 구매한 상태 (사용 중인 상태)
    public final static String STORE_PRODUCT_REFUND = "REFUND";                  // 스토어에서 고객이 환불한 상태 (환불은 CS팀을 통해 가능)
    public final static String STORE_PRODUCT_EXPIRED_PERIOD = "EXPIRED_PERIOD";  // 구매한 상품의 기간이 만료되어 사용 불가능한 상태
    public final static String STORE_PRODUCT_EXPIRED_USAGE = "EXPIRED_USAGE";    // 구매한 상품의 사용량이 만료되어 사용 불가능한 상태

    private ConstDef() {}
}
