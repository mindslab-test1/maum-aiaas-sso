package ai.maum.sso.common;

//import com.sun.org.apache.xpath.internal.operations.String;
import lombok.extern.slf4j.Slf4j;

import java.lang.String;
import javax.servlet.http.HttpServletRequest;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.regex.Pattern;

import static org.springframework.util.ObjectUtils.isEmpty;

@Slf4j
public class Utils {

    /*
    ** 유효기간 초과 여부 확인
    */
    public static boolean isExpiredTime(LocalDateTime dateTime) {
        if(dateTime == null) return true;
        LocalDateTime now = LocalDateTime.now();
        if(now.isAfter(dateTime)) {
            return true;
        }
        return false;
    }

    /**
     * 비밀번호 유효성 체크
     * 특수문자, 숫자, 영문자, 8자 이상
     * 아이디와 동일한 문자 4자 이하
     * 키보드 배열 연속 3자이상 불가
     * @param userId
     * @param password
     * @return
     */
    public static boolean validatePasswordPattern(String userId, String password) {
        boolean flag1 = true;
        boolean flag2 = true;
        boolean flag3 = true;
        boolean flag4 = true;

        if(isEmpty(password))
            return false;

        //chk1 특수문자, 숫자, 영어 1자리 포함, 8자 이상
        String chk1 = "^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,}$";
        flag1 = Pattern.matches(chk1, password);

        //chk2 연속된 문자 4개 허용 안함
        String chk2 = "(\\w)\\1\\1/";
        flag2 = !Pattern.matches(chk2, password);

        //chk3 아이디와 동일하거나, 동일한 4가지 문자 허용 안함
        int cnt = 0;
        for(int i=0; i < userId.length()-3; i++){
            String tmp = null;
            tmp = String.valueOf(userId.charAt(i)) + userId.charAt(i+1) + userId.charAt(i+2) + userId.charAt(i+3);
            if(password.contains(tmp)){
                cnt++;
            }
        }
        if(cnt > 0) flag3 = false;

        //chk4 키보드 배열대로 3자이상 불가
        String val_con0 ="~!@#$%^&*()_+";
        String val_con1 = "`1234567890-=";
        String val_con2 ="QWERTYUIOP{}|";
        String val_con3 ="ASDFGHJKL:\"";
        String val_con4 ="ZXCVBNM<>?";
        String val_con5 ="qwertyuiop[]\\";
        String val_con6 ="asdfghjkl;'";
        String val_con7 ="zxcvbnm,./";

        String[] ifPw = new String[]{
                val_con0
                , new StringBuffer(val_con0).reverse().toString()
                , val_con1
                , new StringBuffer(val_con1).reverse().toString()
                , val_con2
                , new StringBuffer(val_con2).reverse().toString()
                , val_con3
                , new StringBuffer(val_con3).reverse().toString()
                , val_con4
                , new StringBuffer(val_con4).reverse().toString()
                , val_con5
                , new StringBuffer(val_con5).reverse().toString()
                , val_con6
                , new StringBuffer(val_con6).reverse().toString()
                , val_con7
                , new StringBuffer(val_con7).reverse().toString()
        };

        for(int i = 0; i < password.length()-2; i++){
            String tmp = String.valueOf(password.charAt(i)) + password.charAt(i+1) + password.charAt(i+2);
            for (String s : ifPw) {
                if (s.contains(tmp)) {
                    flag4 = false;
                    break;
                }
            }
        }

        return !flag1 | !flag2 | !flag3 | !flag4;
    }

    /**
     * 사용자 IP 조회
     * @param request
     * @return
     */
    public String getUserIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null) ip = request.getHeader("Proxy-Client-IP");
        if (ip == null) ip = request.getHeader("WL-Proxy-Client-IP");
        if (ip == null) ip = request.getHeader("HTTP_CLIENT_IP");
        if (ip == null) ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        if (ip == null) ip = request.getRemoteAddr();
        return ip;
    }

    /**
     * 사용자 비밀번호 암호화
     * @param password
     * @return
     * */
    public String encryptSHA256(String password) {
        String encryptPassword = "";

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(password.getBytes());
            byte byteDate[] = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for(int i=0 ; i<byteDate.length ; i++) {
                stringBuffer.append(Integer.toString((byteDate[i]&0xff) + 0x100, 16).substring(1));
            }
            encryptPassword = stringBuffer.toString();

        } catch (NoSuchAlgorithmException e) {
            log.error(" @ encrypt error ! ==> " + e);
            encryptPassword = null;
        }
        return encryptPassword;
    }
}
