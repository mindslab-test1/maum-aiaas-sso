package ai.maum.sso.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Slf4j
@Service
public class MailSender {

    /**
     * 메일 발송
     * */
    public void postMail(String receiver, String subject, String message, String from) throws MessagingException {
        boolean debug = false;
        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

        String SMTP_HOST_NAME = "gmail-smtp.l.google.com";

        // Properties 설정
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");

        try{
        Authenticator auth = new SMTPAuthenticator();
        Session session = Session.getDefaultInstance(props, auth);

        session.setDebug(debug);

        // create a message
        Message msg = new MimeMessage(session);

        // set the from and to address
        InternetAddress addressFrom = new InternetAddress("마음AI<" + from + ">");
        msg.setFrom(addressFrom);

        InternetAddress addressTo = new InternetAddress(receiver);
        msg.setRecipient(Message.RecipientType.TO, addressTo);

        // Setting the Subject and Content Type
        msg.setSubject(subject);
        msg.setContent(message, "text/html; charset=utf-8");
        Transport.send(msg);
        } catch (Exception e) {
            log.error("@ MailSender.postMail Error !");
            log.error(String.valueOf(e));
        }
    }

    /**
     * 구글 사용자 메일 계정 정보
     * */
    private class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            String username = "sendmail@mindslab.ai";    // gmail 사용자;
            String password = "minds1234";                    // 패스워드;
            return new PasswordAuthentication(username, password);
        }
    }
}
