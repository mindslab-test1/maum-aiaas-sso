package ai.maum.sso.utils;

import ai.maum.sso.domain.UserTEntity;
import ai.maum.sso.property.JwtProperty;
import ai.maum.sso.repository.UserTRepository;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class JwtTokenUtil {

    private final JwtProperty jwtProperty;
    private final UserTRepository userTRepository;

    public String generateAccessToken(String email, Map<String, Object> usableSrvMap) {
        Instant accessTokenExpiredHour = Instant.now().plus(jwtProperty.getAccessTokenExpiredHours(), ChronoUnit.HOURS);

        Date accessTokenExpiredDate = Date.from(accessTokenExpiredHour);

        return Jwts.builder()
                .setSubject(email)
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(accessTokenExpiredDate)
                .claim("email", email)                // 사용자 email
                .claim("name", getUserName(email))    // 사용자 name
                .addClaims(usableSrvMap)                    // 사용자가 사용 가능한 서비스
                .signWith(SignatureAlgorithm.HS256, jwtProperty.getSecret())        // H256과 key로 Sign
                .compact();
    }

    public String generateRefreshToken(String email, Map<String, Object> usableSrvMap) {
        Instant refreshTokenExpiredHour = Instant.now().plus(jwtProperty.getRefreshTokenExpiredDays(), ChronoUnit.DAYS);

        Date refreshTokenExpiredDate = Date.from(refreshTokenExpiredHour);

        return Jwts.builder()
                .setSubject(email)
                .setExpiration(refreshTokenExpiredDate)
                .claim("email", email)                // 사용자 email
                .addClaims(usableSrvMap)                    // 사용자가 사용 가능한 서비스
                .signWith(SignatureAlgorithm.HS256, jwtProperty.getSecret())        // H256과 key로 Sign
                .compact();
    }

    public String extractClaim(String token, String key) {
        Claims claims = Jwts.parser().setSigningKey(jwtProperty.getSecret()).parseClaimsJws(token).getBody();
        Object value = Optional.ofNullable(claims.get(key)).orElseThrow(IllegalArgumentException::new);
        return value.toString();
    }

    /**
     * Jwt 발급시 Subject는 반드시 User의 unique값이다.
     * MaumAI의 경우, email이 unique한 값
     */
    public String extractEmail(String token) {
        Claims claims = Jwts.parser().setSigningKey(jwtProperty.getSecret()).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtProperty.getSecret()).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
            throw new SignatureException("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
            throw new MalformedJwtException("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
            throw new ExpiredJwtException(ex.getHeader(), ex.getClaims(), "Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
            throw new UnsupportedJwtException("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
            throw new IllegalArgumentException("JWT claims string is empty.");
        }
    }

    public String getUserName(String email) {

        UserTEntity userTEntity = userTRepository.findByEmail(email);
        String name = "";

        if(userTEntity != null)
            name = userTEntity.getName();

        return name;
    }
}
