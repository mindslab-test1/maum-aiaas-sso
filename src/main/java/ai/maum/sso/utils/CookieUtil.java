package ai.maum.sso.utils;

import groovy.util.logging.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class CookieUtil {

    @Value("${cookie.secure}")
    private boolean isCookieSecure;

    public void generateCookieForJwtToken(String accessToken, String refreshToken, HttpServletResponse servletServerHttpResponse) {
        Cookie accessTokenCookie = new Cookie("MAUM_AID", accessToken); // access token ID
        accessTokenCookie.setSecure(isCookieSecure); // local,dev: false(http, https 둘 다) / stg,prod: true(https만)
        accessTokenCookie.setHttpOnly(true);
        accessTokenCookie.setPath("/");
        accessTokenCookie.setDomain("maum.ai");
        accessTokenCookie.setMaxAge(-1); // TODO: browser close 때까지 유지

        Cookie refreshTokenCookie = new Cookie("MAUM_RID", refreshToken); // refresh token ID
        refreshTokenCookie.setSecure(isCookieSecure); // local,dev: false(http, https 둘 다) / stg,prod: true(https만)
        refreshTokenCookie.setHttpOnly(true);
        refreshTokenCookie.setPath("/");
        refreshTokenCookie.setDomain("maum.ai");
        refreshTokenCookie.setMaxAge(-1); // TODO: browser close 때까지 유지

        servletServerHttpResponse.addCookie(accessTokenCookie);
        servletServerHttpResponse.addCookie(refreshTokenCookie);
    }

}
