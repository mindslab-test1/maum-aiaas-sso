package ai.maum.sso.exif;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;

@Slf4j
public class SSOInterface {
    /*
    **
    */
    static public void cleanToken(String uri, String client_id, String email, String access_token) {
        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(uri);

            RequestConfig requestConfig = RequestConfig.custom()
                    .setSocketTimeout(2*1000)
                    .setConnectTimeout(2*1000)
                    .setConnectionRequestTimeout(2*1000)
                    .build();
            post.setConfig(requestConfig);

            // 데이터
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("email", new StringBody(email, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("access_token", new StringBody(access_token, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode == 200) {
                log.info("# cleanToken >> client_id={} / email={} >> Response Code : {}", client_id, email, responseCode);
                return;
            }
            else {
                log.error("# cleanToken >> client_id={} / email={} >> Response Code : {}", client_id, email, responseCode);
            }
        } catch(Exception e) {
            log.error("# cleanToken >> client_id={} / email={} >> exception : {}", client_id, email, e.getMessage());
        }
    }
}
