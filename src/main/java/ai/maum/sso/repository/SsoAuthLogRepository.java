package ai.maum.sso.repository;

import ai.maum.sso.domain.SsoAuthLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yejoon3117@mindslab.ai on 2020-09-14
 */
@Repository
public interface SsoAuthLogRepository extends JpaRepository<SsoAuthLogEntity, Integer> {
    SsoAuthLogEntity findByEmailAndAuthKeyAndAuthStatusAndAuthType(String email, String authKey, String authStatus, String authType);
    List<SsoAuthLogEntity> findByEmailAndAuthStatusAndAuthType(String email, String authStatus, String authType);
    SsoAuthLogEntity findByEmailAndAuthStatusAndAuthTypeAndAuthKeyAndCertDateIsNotNull(String email, String authStatus, String code, String authType);
}
