package ai.maum.sso.repository;

import ai.maum.sso.domain.SsoUserHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-18
 */
@Repository
public interface SsoUserHistoryRepository extends JpaRepository<SsoUserHistoryEntity, Integer> {
}
