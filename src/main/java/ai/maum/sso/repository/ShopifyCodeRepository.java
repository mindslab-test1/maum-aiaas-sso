package ai.maum.sso.repository;

import ai.maum.sso.domain.ShopifyCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by yejoon3117@mindslab.ai on 2021-06-04
 * */
public interface ShopifyCodeRepository extends JpaRepository<ShopifyCodeEntity, Integer> {
    ShopifyCodeEntity findByCode(String code);
    List<ShopifyCodeEntity> findByEmailAndStatus(String email, String status);
    ShopifyCodeEntity findByCodeAndEmailAndProductAndModelAndLimit(String code, String email, String product, String model, String limit);
}
