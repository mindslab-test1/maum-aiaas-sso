package ai.maum.sso.repository;

import ai.maum.sso.domain.UserTEntity;
import ai.maum.sso.domain.UserTSimpleInterface;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by kirk@mindslab.ai on 2020-09-14
 */
@Repository
public interface UserTRepository extends JpaRepository<UserTEntity, Integer> {
    UserTEntity findByUserNo(int userNo);
    UserTEntity findByEmail(String email);
    UserTEntity findByApiKey(String apiKey);
    List<String> findByApiIdIsNotNull();
    UserTEntity findByApiId(String id);

    Page<UserTSimpleInterface> findAllByEmailContaining(String email, Pageable pageable);
    Page<UserTSimpleInterface> findAllByNameContaining(String name, Pageable pageable);
    Page<UserTSimpleInterface> findAllByEmailContainingOrNameContaining(String email, String name, Pageable pageable);
}
