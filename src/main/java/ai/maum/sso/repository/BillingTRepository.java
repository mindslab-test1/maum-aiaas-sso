package ai.maum.sso.repository;

import ai.maum.sso.domain.BillingTEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yejoon3117@mindslab.ai on 2021-02-18
 * */
@Repository
public interface BillingTRepository extends JpaRepository<BillingTEntity, Integer> {
    BillingTEntity findByUserNo(int userNo);
}
