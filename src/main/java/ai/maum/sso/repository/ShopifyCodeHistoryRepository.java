package ai.maum.sso.repository;

import ai.maum.sso.domain.ShopifyCodeHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by yejoon3117@mindslab.ai on 2021-06-04
 * */
public interface ShopifyCodeHistoryRepository extends JpaRepository<ShopifyCodeHistoryEntity, Integer> {
}
