package ai.maum.sso.repository;

import ai.maum.sso.domain.OAuthTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by kirk@mindslab.ai on 2020-09-14
 */
@Repository
public interface OAuthTokenRepository extends JpaRepository<OAuthTokenEntity, Integer> {
    OAuthTokenEntity findByState(String state);
    OAuthTokenEntity findByStateAndAuthorizationCode(String state, String code);
    OAuthTokenEntity findByAuthorizationCode(String code);
    OAuthTokenEntity findByRefreshToken(String token);
    OAuthTokenEntity findByAccessToken(String token);
    List<OAuthTokenEntity> findAllByEmail(String email);
    int deleteAllByEmail(String email);
}
