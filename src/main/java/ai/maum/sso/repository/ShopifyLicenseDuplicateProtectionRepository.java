package ai.maum.sso.repository;

import ai.maum.sso.domain.ShopifyLicenseDuplicateProtectionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by yejoon3117@mindslab.ai on 2021-05-27
 * */
public interface ShopifyLicenseDuplicateProtectionRepository extends JpaRepository<ShopifyLicenseDuplicateProtectionEntity, Integer> {
}
