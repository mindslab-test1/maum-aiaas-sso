package ai.maum.sso.repository;

import ai.maum.sso.domain.ShopifyLicenseEntity;
import ai.maum.sso.domain.ShopifyLicenseManagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by yejoon3117@mindslab.ai on 2021-05-11
 * */
public interface ShopifyLicenseManagementRepository extends JpaRepository<ShopifyLicenseManagementEntity, Integer> {
    ShopifyLicenseManagementEntity findByLicenseKeyAndActive(String licenseKey, int i);
}
