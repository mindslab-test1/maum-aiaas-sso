package ai.maum.sso.repository;

import ai.maum.sso.domain.OAuthClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kirk@mindslab.ai on 2020-09-14
 */
@Repository
public interface OAuthClientRepository extends JpaRepository<OAuthClientEntity, Integer> {
    OAuthClientEntity findByClientId(String clientId);
}
