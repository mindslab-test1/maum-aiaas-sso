package ai.maum.sso.repository;

import ai.maum.sso.domain.SsoUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yejoon3117@mindslab.ai on 2020-09-14
 */
@Repository
public interface SsoUserRepository extends JpaRepository<SsoUserEntity, Integer> {
    SsoUserEntity findByEmailAndPassword(String email, String password);
    SsoUserEntity findByEmail(String email);
}
