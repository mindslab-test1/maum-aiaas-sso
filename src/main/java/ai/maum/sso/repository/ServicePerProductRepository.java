package ai.maum.sso.repository;

import ai.maum.sso.domain.ServicePerProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yejoon3117@mindslab.ai on 2021-02-17
 * */
@Repository
public interface ServicePerProductRepository extends JpaRepository<ServicePerProductEntity, Integer> {
    ServicePerProductEntity findByProduct(int product);
}
