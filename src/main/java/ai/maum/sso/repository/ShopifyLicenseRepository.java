package ai.maum.sso.repository;

import ai.maum.sso.domain.ShopifyLicenseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by yejoon3117@mindslab.ai on 2021-05-11
 * */
public interface ShopifyLicenseRepository extends JpaRepository<ShopifyLicenseEntity, Integer> {
    List<ShopifyLicenseEntity> findByEmail(String email);
    ShopifyLicenseEntity findByEmailAndLicenseKey(String email, String licenseKey);
}
