package ai.maum.sso.domain;

/**
 * Created by kirk@mindslab.ai on 2021-01-11
 */
public interface UserTSimpleInterface {
    String getEmail();
    String getName();
}
