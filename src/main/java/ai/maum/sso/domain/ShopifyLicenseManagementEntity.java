package ai.maum.sso.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "SHOPIFY_LICENSE_MANAGEMENT")

public class ShopifyLicenseManagementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "LICENSE_KEY")
    private String licenseKey;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ACTIVE")
    private int active;
}
