package ai.maum.sso.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-18
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "SSO_USER_HISTORY")
public class SsoUserHistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "REG_DATE")
    private LocalDateTime regDate;

}
