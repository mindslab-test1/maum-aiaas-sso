package ai.maum.sso.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "SHOPIFY_CODE_HISTORY")
public class ShopifyCodeHistoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PRODUCT")
    private String product;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "LIMIT")
    private String limit;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "REG_DATE")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime regDate;

}
