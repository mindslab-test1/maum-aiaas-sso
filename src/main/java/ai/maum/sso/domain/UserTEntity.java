package ai.maum.sso.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-09-14
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "User_T")
public class UserTEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UserNo")
    private Integer userNo;

    @Nationalized
    @Column(name = "Name")
    private String name;

    @Nationalized
    @Column(name = "ID")
    private String id;

    @Nationalized
    @Column(name = "Password")
    private String password;

    @Nationalized
    @Column(name = "Email")
    private String email;

    @Nationalized
    @Column(name = "Phone")
    private String phone;

    @Column(name = "Grade")
    private Integer grade;

    @Column(name = "Product")
    private Integer product;

    @Column(name = "Enabled")
    private Integer enabled;

    @Nationalized
    @Column(name = "Authority")
    private String authority;

    @Column(name = "Type")
    private Integer type;

    @Column(name = "Limit")
    private Integer limit;

    @Nationalized
    @Column(name = "Account")
    private String account;

    @Column(name = "VoiceAgree")
    private Integer voiceAgree;

    @Column(name = "PrivacyAgree")
    private Integer privacyAgree;

    @Column(name = "PrivacyDate")
    private LocalDateTime privacyDate;

    @Column(name = "LastLoginDate")
    private LocalDateTime lastLoginDate;

    @Column(name = "LastLogoutDate")
    private LocalDateTime lastLogoutDate;

    @Column(name = "djangoid")
    private Integer djangoid;

    @Column(name = "Active")
    private Integer active;

    @Column(name = "CreateDate")
    private LocalDateTime createDate;

    @Column(name = "UpdateDate")
    private LocalDateTime updateDate;

    @Column(name = "UpdateUser")
    private Integer updateUser;

    @Nationalized
    @Column(name = "Lkey")
    private String lkey;

    @Nationalized
    @Column(name = "Gkey")
    private String gkey;

    @Column(name = "LoginFailCNT")
    private Integer loginFailCnt;

    @Nationalized
    @Column(name = "NationCD")
    private String nationCd;

    @Nationalized
    @Column(name = "Company")
    private String company;

    @Nationalized
    @Column(name = "Job")
    private String job;

    @Column(name = "CreateFlag")
    private Integer createFlag;

    @Nationalized
    @Column(name = "CompanyEmail")
    private String companyEmail;

    @Nationalized
    @Column(name = "ApiKey")
    private String apiKey;

    @Nationalized
    @Column(name = "ApiId")
    private String apiId;

    @Column(name = "MarketingAgree")
    private Integer marketingAgree;

    @Nationalized
    @Column(name = "RegisterPath")
    private String registerPath;

    @Column(name = "MarketingDate")
    private LocalDateTime marketingDate;

    @Column(name = "Status")
    private Integer status;

    @Column(name = "PayCount")
    private Integer payCount;

    @Column(name = "Channel")
    private String channel;

}
