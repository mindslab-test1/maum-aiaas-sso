package ai.maum.sso.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Created by kirk@mindslab.ai on 2020-09-14
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "OAUTH_CLIENT")
public class OAuthClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "callback_uri")
    private String callbackUri;

    @Column(name = "logout_noti_uri")
    private String logoutNotiUri;

    @Column(name = "flag_create_member")
    private int flagCreateMember;
}
