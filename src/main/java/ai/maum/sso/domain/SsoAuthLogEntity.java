package ai.maum.sso.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by yejoon3117@mindslab.ai on 2020-09-14
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "SSO_AUTHLOG")
public class SsoAuthLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "NAME")
    private String name;

    @Column(name = "AUTH_TYPE")
    private String authType;

    @Column(name = "AUTH_STATUS")
    private String authStatus;

    @Column(name = "AUTH_KEY")
    private String authKey;

    @Column(name = "EXPIRE_DATE")
    private LocalDateTime expireDate;

    @Column(name = "REG_DATE")
    private LocalDateTime regDate;

    @Column(name = "MDF_DATE")
    private LocalDateTime mdfDate;

    @Column(name = "CERT_DATE")
    private LocalDateTime certDate;
}
