package ai.maum.sso.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "SERVICE_PER_PRODUCT")
public class ServicePerProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "PRODUCT")
    private int product;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CLOUD_API")
    private int cloud_api;

    @Column(name = "AI_BUILDER")
    private int ai_builder;

    @Column(name = "MINUTES")
    private int minutes;

    @Column(name = "FAST_AI")
    private int fast_ai;

    @Column(name = "AVA")
    private int ava;

    @Column(name = "DATA")
    private int data;

    @Column(name = "ACADEMY")
    private int academy;
}
