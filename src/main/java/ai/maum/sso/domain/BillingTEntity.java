package ai.maum.sso.domain;

import lombok.*;
import reactor.util.annotation.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "Billing_T")
public class BillingTEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "BillKey")
    private String billKey;

    @Column(name = "Product")
    private int product;

    @Column(name = "Payment")
    private String payment;

    @Column(name = "Issuer")
    private String issuer;

    @Column(name = "Number")
    private String number;

    @Column(name = "PaymentDate")
    private LocalDateTime paymentDate;

    @Column(name = "userNo")
    private int userNo;

    @Column(name = "active")
    private int active;

    @Column(name = "CreateDate")
    private LocalDateTime createDate;

    @Column(name = "CreateUser")
    private int createUser;

    @Column(name = "UpdateDate")
    private LocalDateTime updateDate;

    @Column(name = "UpdateUser")
    private Integer updateUser;

    @Column(name = "FailReason")
    private String failReason;

    @Column(name = "Description")
    private String description;
}
