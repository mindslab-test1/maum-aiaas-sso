package ai.maum.sso.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Configuration;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by yejoon3117@mindslab.ai on 2020-09-14
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "SSO_USER")
public class SsoUserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "REGISTER_PATH")
    private String registerPath;

    @Column(name = "TERMS_AGREE")
    private Integer termsAgree;

    @Column(name = "PRIVACY_AGREE")
    private Integer privacyAgree;

    @Column(name = "MARKETING_AGREE")
    private Integer marketingAgree;

    @Column(name = "REG_DATE")
    private LocalDateTime regDate;

    @Column(name = "MDF_DATE")
    private LocalDateTime mdfDate;

    @Column(name = "UPDATE_PASSWORD_DATE")
    private LocalDateTime updatePasswordDate;

    @Column(name = "PRIVACY_AGREE_DATE")
    private LocalDateTime privacyAgreeDate;

    @Column(name = "ACCOUNT_STATUS")
    private String accountStatus;

    @Column(name = "LAST_LOGIN")
    private LocalDateTime lastLogin;

    @Column(name = "COMPANY")
    private String company;

}
