package ai.maum.sso.controller;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.common.data.CommonMsg;
import ai.maum.sso.common.data.CommonResponse;
import ai.maum.sso.domain.ShopifyCodeEntity;
import ai.maum.sso.domain.ShopifyLicenseEntity;
import ai.maum.sso.service.ShopifyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
public class ShopifyController {

    private final ShopifyService shopifyService;

    /* -------------------------------------------------------------------------------------------------------------- */
    /* 상품 코드 관련 API */
    /* -------------------------------------------------------------------------------------------------------------- */
    /** 구매한 고객의 상품 코드 등록 - 2021. 06. 04 - LYJ */
    @RequestMapping(value = "/shopify/insertCode", method={RequestMethod.POST})
    @ResponseBody
    public CommonResponse<Object> insertCode(@RequestParam(value = "code") String code,
                                             @RequestParam(value = "email") String email,
                                             @RequestParam(value = "product") String product,
                                             @RequestParam(value = "model") String model,
                                             @RequestParam(value = "limit") String limit) throws Exception {
        log.info(" @ Hello ShopifyController.insertCode ! ");

        int result = shopifyService.insertCode(code, email, product, model, limit);

        CommonResponse<Object> response = new CommonResponse<>();
        if(result > 0) {
            response.setCode(CommonMsg.ERR_CODE_SUCCESS);
            response.setMsg(CommonMsg.ERR_MSG_SUCCESS);
        } else if(result == -1) {                                       // 등록되어 있는 code의 경우 에러 처리
            response.setCode(CommonMsg.ERR_CODE_DUPLICATE_LICENSE);
            response.setMsg(CommonMsg.ERR_MSG_DUPLICATE_LICENSE);
        } else {
            response.setCode(CommonMsg.ERR_CODE_FAILURE);
            response.setMsg(CommonMsg.ERR_MSG_FAILURE);
        }
        return response;
    }

    /** 만료되지 않은 상품 코드 조회 - 2021. 06. 04 - LYJ */
    @RequestMapping(value = "/shopify/getValidCodeList", method = {RequestMethod.POST})
    @ResponseBody
    public CommonResponse<Object> getValidCodeList(@RequestParam(value = "email") String email) throws Exception {
        log.info(" @ Hello ShopifyController.getValidCodeList ! ");

        CommonResponse<Object> response = new CommonResponse<>();

        List<ShopifyCodeEntity> shopifyCodeEntityList = shopifyService.getValidCodeList(email);

        if(shopifyCodeEntityList != null) {
            response.setCode(CommonMsg.ERR_CODE_SUCCESS);
            response.setMsg(CommonMsg.ERR_MSG_SUCCESS);
            if(shopifyCodeEntityList.size() == 0) {
                response.setData(null);
            } else {
                response.setData(shopifyCodeEntityList);
            }
        } else {
            response.setCode(CommonMsg.ERR_CODE_FAILURE);
            response.setMsg(CommonMsg.ERR_MSG_FAILURE);
        }
        return response;
    }

    /** 상품 코드 status 변경 - 2021. 06. 04 - LYJ */
    @RequestMapping(value = "/shopify/updateCodeStatus", method = {RequestMethod.POST})
    @ResponseBody
    public CommonResponse<Object> updateProductCodeStatus(@RequestParam(value = "email") String email,
                                                          @RequestParam(value = "code") String code,
                                                          @RequestParam(value = "product") String product,
                                                          @RequestParam(value = "model") String model,
                                                          @RequestParam(value = "limit") String limit,
                                                          @RequestParam(value = "status") String status) throws Exception {
        log.info(" @ Hello ShopifyController.updateProductCodeStatus ! ");

        CommonResponse<Object> response = new CommonResponse<>();

        // 약속된 status 인지 확인
        if(ConstDef.STORE_PRODUCT_BUY.equals(status) == false
                && ConstDef.STORE_PRODUCT_REFUND.equals(status) == false
                && ConstDef.STORE_PRODUCT_EXPIRED_PERIOD.equals(status) == false
                && ConstDef.STORE_PRODUCT_EXPIRED_USAGE.equals(status) == false) {
            response.setCode(CommonMsg.ERR_CODE_INVALID_PARAMS);
            response.setMsg(CommonMsg.ERR_MSG_INVALID_PARAMS + ": status");
            return response;
        }

        int result = shopifyService.updateCodeStatus(email, code, product, model, limit, status);
        if(result > 0) {
            response.setCode(CommonMsg.ERR_CODE_SUCCESS);
            response.setMsg(CommonMsg.ERR_MSG_SUCCESS);
        } else if (result == -1) {
            response.setCode(CommonMsg.ERR_CODE_NO_MATCHING_DATA);
            response.setMsg(CommonMsg.ERR_MSG_NO_MATCHING_DATA);
        } else {
            response.setCode(CommonMsg.ERR_CODE_FAILURE);
            response.setMsg(CommonMsg.ERR_MSG_FAILURE);
        }

        return response;
    }



    /* -------------------------------------------------------------------------------------------------------------- */
    /* 라이센스 관련 API - 임시방편 API로 곧 사용 안 할 예정 */
    /* -------------------------------------------------------------------------------------------------------------- */
    @RequestMapping(value = "/shopify/getLicenseList", method={RequestMethod.POST})
    @ResponseBody
    public List<ShopifyLicenseEntity> getLicenseListByEmail(@RequestParam(value="email") String email) throws Exception {
        log.info(" @ Hello ShopifyApiController.getLicenseListByEmail ! ");
        List<ShopifyLicenseEntity> shopifyLicenseVoList = shopifyService.getLicenseListByEmail(email);

        return shopifyLicenseVoList;
    }

    @RequestMapping(value = "/shopify/insertLicense", method = {RequestMethod.POST})
    @ResponseBody
    public CommonResponse<Object> insertShopifyLicense(@RequestParam(value="email") String email,
                                                       @RequestParam(value="license_key") String licenseKey) throws Exception {
        log.info(" @ Hello ShopifyApiController.insertShopifyLicense ! ");

        ShopifyLicenseEntity shopifyLicenseEntity = new ShopifyLicenseEntity();
        shopifyLicenseEntity.setEmail(email);
        shopifyLicenseEntity.setLicenseKey(licenseKey);

        CommonResponse<Object> resp = new CommonResponse<>();

        int checkVal = shopifyService.checkUserLicense(shopifyLicenseEntity);

        // 특정 라이센스키만 중복 등록 제한되도록 설정
        if(checkVal > 0) {

            if (shopifyService.insertShopifyLicense(shopifyLicenseEntity) > 0) {
                resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
                resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);
            } else {
                resp.setCode(CommonMsg.ERR_CODE_FAILURE);
                resp.setMsg(CommonMsg.ERR_MSG_FAILURE);
            }
        } else {    // 사용자가 중복되면 안 되는 license key를 등록할 경우
            resp.setCode(CommonMsg.ERR_CODE_DUPLICATE_LICENSE);
            resp.setMsg(CommonMsg.ERR_MSG_DUPLICATE_LICENSE);
        }
        return  resp;
    }

    @RequestMapping(value = "/shopify/validateLicense", method = {RequestMethod.POST})
    @ResponseBody
    public CommonResponse<Object> validateLicense(@RequestParam(value="license_key") String license_key) throws Exception {

        log.info(" @ Hello ShopifyApiController.validateLicense ! ");

        CommonResponse<Object> resp = new CommonResponse<>();

        if(shopifyService.validateLicense(license_key) > 0) {
            resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
            resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);
        } else {
            resp.setCode(CommonMsg.ERR_CODE_INVALID_LICENSE);
            resp.setMsg(CommonMsg.ERR_MSG_INVALID_LICENSE);
        }
        return  resp;
    }
}
