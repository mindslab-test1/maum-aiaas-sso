package ai.maum.sso.controller;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.common.Utils;
import ai.maum.sso.common.data.CommonResponse;
import ai.maum.sso.domain.SsoUserEntity;
import ai.maum.sso.domain.UserTEntity;
import ai.maum.sso.dto.UserDto;
import ai.maum.sso.service.MaumRestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MaumLoginRestController {

    private final MaumRestService service;

    /** ------------------------------------------------------------------------------------------------------------ */
    /** 로그인 */
    /** ------------------------------------------------------------------------------------------------------------ */
    
    @RequestMapping(value = "/maum/authorize", method = {RequestMethod.GET, RequestMethod.POST})
    public CommonResponse<Object> maumLogin(@RequestParam("email") String email,
                                            @RequestParam("userPw") String userPw,
                                            @RequestParam("response_type") String response_type,
                                            @RequestParam("client_id") String client_id,
                                            @RequestParam("redirect_uri") String redirect_uri,
                                            @RequestParam("state") String state,
                                            HttpServletRequest request, HttpServletResponse response, Model model) {

        String codeMaumHQ = null;

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/maum/authorize");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그인을 위한 권한 요청 (자체 로그인 수행)");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "response_type", response_type);
        log_msg += String.format(":: %-20s = %s\n", "client_id", client_id);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if(cookie.getName().equals(ConstDef.COOKIE_NAME___MAUM_HQ___STATE)) {
                    log.info("@ OAuth.token : Cookie.name={},{}", cookie.getName(), cookie.getValue());
                    codeMaumHQ = cookie.getValue();
                }
            }
        }

        Utils utils = new Utils();
        String encryptPW = utils.encryptSHA256(userPw);
        CommonResponse<Object> result = service.authorize(email, encryptPW, codeMaumHQ, client_id, redirect_uri, state, response);

        /* 자체 로그인 인증 관련 구조체 반환 */
        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "desc", "계정 인증 (자체 로그인 수행)");
        log_msg += String.format(":: %-20s = %s\n", "auth result", result.toString());
        log_msg += String.format(":: %-20s = %s\n", "email", email);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri);

        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        return result;
    }

    /* 통합로그인 적용 */
    @RequestMapping(value = "/maum/oauth/authorize", method = {RequestMethod.GET, RequestMethod.POST})
    public CommonResponse<Object> maumSsoLogin(@RequestParam("email") String email,
                                            @RequestParam("userPw") String userPw,
                                            @RequestParam("response_type") String response_type,
                                            @RequestParam("client_id") String client_id,
                                            @RequestParam("redirect_uri") String redirect_uri,
                                            @RequestParam("state") String state,
                                            HttpServletRequest request, HttpServletResponse response, Model model) {

        String codeMaumHQ = null;

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/maum/oauth/authorize");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그인을 위한 권한 요청 (자체 로그인 수행)");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "response_type", response_type);
        log_msg += String.format(":: %-20s = %s\n", "client_id", client_id);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if(cookie.getName().equals(ConstDef.COOKIE_NAME___MAUM_HQ___STATE)) {
                    log.info("@ OAuth.token : Cookie.name={},{}", cookie.getName(), cookie.getValue());
                    codeMaumHQ = cookie.getValue();
                }
            }
        }

        Utils utils = new Utils();
        String encryptPW = utils.encryptSHA256(userPw);
        CommonResponse<Object> result = service.ssoAuthorize(email, encryptPW, codeMaumHQ, client_id, redirect_uri, state, response);

        /* 자체 로그인 인증 관련 구조체 반환 */
        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "desc", "계정 인증 (자체 로그인 수행)");
        log_msg += String.format(":: %-20s = %s\n", "auth result", result.toString());
        log_msg += String.format(":: %-20s = %s\n", "email", email);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        return result;
    }

    /* state 값 체크 */
    @RequestMapping(value = "/maum/checkState")
    public Boolean checkState(@RequestParam("state") String state,
                              HttpServletRequest request, HttpServletResponse response, Model model) {
        if( service.checkState(state) == null ) {
            return true;
        } else {
            return false;
        }
    }

    /** ------------------------------------------------------------------------------------------------------------ */
    /** 회원가입, 비밀번호 찾기 */
    /** ------------------------------------------------------------------------------------------------------------ */
    
    /* (회원가입) SSO_USER, maum.ai user_t table 이메일 중복 검사 */
    @RequestMapping(value = "/maum/checkUserEmail", method = RequestMethod.POST)
    public Boolean checkDuplicateEmail(@RequestParam("email") String email, HttpServletRequest request) {

        HttpSession httpSession = request.getSession(true);

        //SSO_USER table 이메일 중복 검사
        SsoUserEntity resultSsoUser = service.checkDuplicateEmail(email);

        // maum.ai User_T table 이메일 중복 검사
        // 구글로 로그인한 사용자가 구글 이메일을 사용하여 자체 회원으로 가입하는 경우 제한!
        UserTEntity resultUserT = service.checkUserEmail(email);

        if( resultSsoUser==null && resultUserT==null ) // 가입 가능
            return true;
        else                                            // 가입 불가능
            return false;
    }

    /* (비밀번호 찾기) 가입한 이메일인지 확인 */
    @RequestMapping(value = "/maum/checkPwEmail", method = RequestMethod.POST)
    public Boolean checkPwEmail(@RequestParam("email") String email, HttpServletRequest request) {

        HttpSession httpSession = request.getSession(true);

        //SSO_USER table 이메일 중복 검사
        SsoUserEntity resultSsoUser = service.checkDuplicateEmail(email);

        // maum.ai User_T table 이메일 중복 검사
        UserTEntity resultUserT = service.checkUserEmail(email);

        if( (resultSsoUser!=null) && (resultUserT!=null) ) // 비밀번호 찾기 가능
            return true;
        else                                            // 비밀번호 찾기 불가능
            return false;
    }

    /* 인증 코드 메일로 전송 */
    @RequestMapping(value = "/maum/sendCertMail", method = RequestMethod.POST)
    public void sendCertMail(@RequestParam("email") String email,
                             @RequestParam("type") String type,
                             HttpServletRequest request) throws MessagingException {

        HttpSession httpSession = request.getSession(true);

        service.sendCertMail(email, type);
    }
    
    /* 인증 코드 일치 확인 */
    @RequestMapping(value = "/maum/checkCertCode", method = RequestMethod.POST)
    public CommonResponse<Object> checkCertCode(@RequestParam("email") String email,
                                                @RequestParam("code") String code,
                                                @RequestParam("type") String type,
                                                HttpServletRequest request){
        log.info(" @ Hello checkCertCode ! email : {}, code : {}, type : {}", email, code, type);

        HttpSession httpSession = request.getSession(true);

        CommonResponse<Object> response = service.checkCertCode(email, code, type);
        return response;
    }

    /* 회원가입 */
    @RequestMapping(value = "/maum/registerUser", method = RequestMethod.POST)
    public CommonResponse<Object> registerUser(@RequestParam("email") String email,
                                               @RequestParam("code") String code,
                                               @RequestParam("password") String password,
                                               @RequestParam("name") String name,
                                               @RequestParam("phone") String phone,
                                               @RequestParam("company") String company,
                                               @RequestParam("registerPath") String registerPath,
                                               @RequestParam("marketingAgree") int marketingAgree,
                                               HttpServletRequest request) throws Exception {

        HttpSession httpSession = request.getSession(true);

        // 비밀번호 암호화
        Utils utils = new Utils();
        String encryptPW = utils.encryptSHA256(password);
        
        CommonResponse<Object> userEntity = service.registerUser(email, code, encryptPW, name, phone, company, registerPath, marketingAgree);

        return userEntity;
    }

    /* 비밀번호 변경 */
    @RequestMapping(value = "/maum/changePW", method = RequestMethod.POST)
    public CommonResponse<Object> changePW(@RequestParam("email") String email,
                                           @RequestParam("newPassword") String newPassword,
                                           HttpServletRequest request) {
        HttpSession httpSession = request.getSession(true);

        // 비밀번호 암호화
        Utils utils = new Utils();
        String encryptPW = utils.encryptSHA256(newPassword);

        CommonResponse<Object> result = service.changePW(email, encryptPW);

        return result;
    }

    @RequestMapping(value = "/maum/userList", method = RequestMethod.GET)
    public ResponseEntity<Iterable<UserDto>> list(@RequestParam(value = "code") String code,
                                                  @RequestParam(value = "type", required = false, defaultValue = "ALL") String type,
                                                  @RequestParam(value = "sch", required = false, defaultValue = "") String sch,
                                                  Pageable pageable) {
        if(!"MINDS_SSO".equalsIgnoreCase(code)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        log.info(" @ list : type {}, sch : {}", type, sch);
        Iterable<UserDto> list = service.userList(type, sch, pageable);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
