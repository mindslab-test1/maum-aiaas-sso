package ai.maum.sso.controller;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.common.Utils;
import ai.maum.sso.domain.OAuthTokenEntity;
import ai.maum.sso.domain.UserTEntity;
import ai.maum.sso.repository.OAuthTokenRepository;
import ai.maum.sso.repository.UserTRepository;
import ai.maum.sso.service.MaumRestService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Controller
@RequiredArgsConstructor
public class MaumLoginController {

    @Value("${url.maum-ai}")
    String maumUrl;

    @Value("${my.domain}")
    String ssoUrl;

    private final OAuthTokenRepository oAuthTokenRepository;
    private final UserTRepository userTRepository;
    private final MaumRestService maumRestService;

    private static final Logger logger = LoggerFactory.getLogger(MaumLoginController.class);

    //	로그인 페이지
    @RequestMapping(value = "/maum/loginMain", method = RequestMethod.GET)
    public String login(@RequestParam(value = "response_type") String response_type,
                        @RequestParam(value = "client_id") String client_id,
                        @RequestParam(value = "redirect_uri") String redirect_uri,
                        //@RequestParam(value = "state") String state,
                        HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        logger.info("@ Hello maum loginMain() - lang : " + request.getLocale().toString());

        // 로그인 여부 확인 - 쿠키가 존재하면, 재로그인으로 바로 콜백 호출
        String codeMaumHQ = null;
        OAuthTokenEntity tokenEntity;
        // 쿠키 조회
        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if(cookie.getName().equals(ConstDef.COOKIE_NAME___MAUM_HQ___STATE)) {
                    logger.info("@ OAuth.token : Cookie.name={},{}", cookie.getName(), cookie.getValue());
                    codeMaumHQ = cookie.getValue();
                }
            }
        }
        // 로그인 되어 있는 사용자
        if (codeMaumHQ != null && (tokenEntity = oAuthTokenRepository.findByState(codeMaumHQ)) != null && Utils.isExpiredTime(tokenEntity.getRefreshExpireDatetime()) == false) {
            String callbackUrl = null;

            // 사용자 이메일 조회
            String email = tokenEntity.getEmail();
            UserTEntity userTEntity = userTRepository.findByEmail(email);

            // 계정 채널 조회
            if("maum".equalsIgnoreCase(userTEntity.getChannel())) {
                logger.info("=====>> SSO MaumLoginController.login (자체) : 재로그인!");
                callbackUrl = maumRestService.procMaumCallback(tokenEntity.getAuthorizationCode(), codeMaumHQ, redirect_uri);
            } else {
                logger.info("=====>> SSO MaumLoginController.login (구글) : 재로그인!");
                callbackUrl =  redirect_uri + "?state=" + codeMaumHQ + "&code=" + tokenEntity.getAuthorizationCode();
            }
            logger.info(" @ REDIRECT: {} / state={}", callbackUrl, codeMaumHQ);
            return "redirect:" + callbackUrl;
        }

        // 로그인되어 있지 않은 경우
        else {
            model.addAttribute("response_type", response_type);
            model.addAttribute("client_id", client_id);
            model.addAttribute("redirect_uri", redirect_uri);
            model.addAttribute("state", UUID.randomUUID());

            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals("lang")) {
                        logger.info("language - " + cookie.getValue());
                    }
                }
            }
            return "/login/loginMain";
        }
    }

    //	로그인 페이지 - JWT가 적용된 기능 사용하는 페이지
    @RequestMapping(value = "/maum/oauthLoginMain", method = RequestMethod.GET)
    public String oauthLogin(@RequestParam(value = "response_type") String response_type,
                        @RequestParam(value = "client_id") String client_id,
                        @RequestParam(value = "redirect_uri") String redirect_uri,
                        //@RequestParam(value = "state") String state,
                        HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        logger.info("@ Hello maum oauthLogin() - lang : " + request.getLocale().toString());

        String codeMaumHQ = null;
        OAuthTokenEntity tokenEntity;

        // 쿠키 조회
        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if(cookie.getName().equals(ConstDef.COOKIE_NAME___MAUM_HQ___STATE)) {
                    logger.info("@ OAuth.token : Cookie.name={},{}", cookie.getName(), cookie.getValue());
                    codeMaumHQ = cookie.getValue();
                }
            }
        }

        model.addAttribute("response_type", response_type);
        model.addAttribute("client_id", client_id);
        model.addAttribute("redirect_uri", redirect_uri);
        model.addAttribute("state", UUID.randomUUID());

        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("lang")) {
                    logger.info("language - " + cookie.getValue());
                }
            }
        }
        return "/login/oauthLoginMain";
    }

    //	약관 동의 페이지
    @RequestMapping(value = "/maum/agreeMain", method = RequestMethod.GET)
    public String agree(@RequestParam(value = "redirect_uri") String redirect_uri,
                        @RequestParam(value="error", required=false) String error,
                        HttpServletRequest request, HttpServletResponse response, Model model) {

        logger.info("@ Hello maum agreeMain()");

        model.addAttribute("redirect_uri", redirect_uri);
        model.addAttribute("error", error);
        model.addAttribute("errormsg", request.getParameter("errormsg"));

        return "/login/agreeMain";
    }

    //	회원가입 페이지
    @RequestMapping(value = "/maum/signUpMain", method = RequestMethod.POST)
    public String signUp(@RequestParam(value = "marketingAgree") int marketingAgree,
                         @RequestParam(value = "redirect_uri") String redirect_uri,
                         @RequestParam(value="error", required=false) String error,
                         HttpServletRequest request, HttpServletResponse response, Model model) {

        logger.info("@ Hello maum signUpMain()");

        model.addAttribute("marketingAgree", marketingAgree);
        model.addAttribute("redirect_uri", redirect_uri);
        model.addAttribute("error", error);
        model.addAttribute("errormsg", request.getParameter("errormsg"));

        return "/login/signUpMain";
    }

    //  회원가입 완료 페이지
    @RequestMapping(value = "/maum/signUpCompleteMain", method = RequestMethod.POST)
    public String signUpComplete(@RequestParam(value = "email") String email,
                                 @RequestParam(value = "name") String name,
                                 @RequestParam(value = "redirect_uri") String redirect_uri,
                                 @RequestParam(value = "error", required = false) String error,
                                 HttpServletRequest request, HttpServletResponse response, Model model) {


        logger.info("@ Hello maum signUpComplete");

        model.addAttribute("maumUrl", maumUrl);
        model.addAttribute("name", name);
        model.addAttribute("email", email);
        model.addAttribute("redirect_uri", maumUrl + "/maum/user/info");
        model.addAttribute("error", error);
        model.addAttribute("errormsg", request.getParameter("errormsg"));

        return "/login/signUpCompleteMain";
    }

    //	결제정보 등록 페이지 --> 사용 안 함
    @RequestMapping(value = "/maum/registrationMain", method = RequestMethod.GET)
    public String registration(@RequestParam(value="error", required=false) String error,
                               HttpServletRequest request, HttpServletResponse response, Model model) {

        logger.info("@ Hello maum registrationMain()");

        model.addAttribute("error", error);
        model.addAttribute("errormsg", request.getParameter("errormsg"));

        return "/login/registration";
    }

    //	비밀번호 찾기 페이지
    @RequestMapping(value = "/maum/passwordMain", method = RequestMethod.GET)
    public String password(@RequestParam(value = "response_type") String response_type,
                           @RequestParam(value = "client_id") String client_id,
                           @RequestParam(value = "redirect_uri") String redirect_uri,
                           @RequestParam(value = "state") String state,
                           @RequestParam(value="error", required=false) String error,
                           HttpServletRequest request, HttpServletResponse response, Model model) {

        logger.info("@ Hello maum passwordMain()");

        model.addAttribute("sso_url", ssoUrl);
        model.addAttribute("response_type", response_type);
        model.addAttribute("client_id", client_id);
        model.addAttribute("redirect_uri", redirect_uri);
        model.addAttribute("state", state);
        model.addAttribute("error", error);
        model.addAttribute("errormsg", request.getParameter("errormsg"));

        return "/login/passwordMain";
    }
}
