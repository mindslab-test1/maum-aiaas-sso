package ai.maum.sso.controller;

import ai.maum.sso.common.data.CommonMsg;
import ai.maum.sso.common.data.CommonResponse;
import ai.maum.sso.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Slf4j
@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @RequestMapping(value = "/user/getStatus", method={RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<CommonResponse> getStatus(@RequestParam(value = "email") String email) throws Exception {
        log.debug(" @ Hello UserController.getStatus ! ");
        try {

            CommonResponse<Object> result = new CommonResponse<>();
            HashMap<String, Object> resultMap = new HashMap<>();

            resultMap = userService.getUserStatus(email);

            if(resultMap == null) {
                result.setData(null);
            } else {
                result.setData(resultMap);
            }

            return ResponseEntity.status(HttpStatus.OK).body(result);

        } catch(Exception e) {

            log.error("/user/getStatus Method Exception \n" + e.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CommonResponse<Object>(CommonMsg.ERR_CODE_NOT_FOUND, CommonMsg.ERR_MSG_NOT_FOUND));

        }
    }
}
