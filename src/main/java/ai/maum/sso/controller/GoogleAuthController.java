package ai.maum.sso.controller;

import ai.maum.sso.common.ConstDef;
import ai.maum.sso.domain.OAuthTokenEntity;
import ai.maum.sso.repository.OAuthTokenRepository;
import ai.maum.sso.service.GoogleAuthService;
import ai.maum.sso.utils.CookieUtil;
import ai.maum.sso.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Controller
@RequiredArgsConstructor
public class GoogleAuthController {

    private final GoogleAuthService googleAuthService;
    private final OAuthTokenRepository oAuthRepository;
    private final CookieUtil cookieUtil;
    private final JwtTokenUtil jwtTokenUtil;
    private final RedisTemplate redisTemplate;

    @Value("${my.domain}")
    String MY_DOMAIN;

    private final String MAUM_AI___SIGNUP_PATH = "/member/signupForm";

    /*
    ** 로그인을 위한 권한 요청 (구글 로그인 수행)
    */
    @RequestMapping("/hq/oauth/authorize")
    public String login(HttpServletRequest request,
                        HttpServletResponse response,
                        @RequestParam("response_type") String response_type,
                        @RequestParam("client_id")  String client_id,
                        @RequestParam("redirect_uri")  String redirect_uri,
                        @RequestParam("state")  String state
                        ) {
        String codeMaumHQ = null;

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/authorize");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그인을 위한 권한 요청 (구글 로그인 수행)");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "response_type", response_type);
        log_msg += String.format(":: %-20s = %s\n", "client_id", client_id);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if(cookie.getName().equals(ConstDef.COOKIE_NAME___MAUM_HQ___STATE)) {
                    log.info("@ OAuth.token : Cookie.name={},{}", cookie.getName(), cookie.getValue());
                    codeMaumHQ = cookie.getValue();
                }
            }
        }

        String returnUri = googleAuthService.authorize(response, codeMaumHQ, response_type, client_id, redirect_uri, state);

        /* 구글 인증 REDIRECTION URL 반환 */
        log.info("@ oauth/authorize : redirect_uri={}", returnUri);
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); /* HTTP 1.1. */
        response.setHeader("Pragma", "no-cache"); /* HTTP 1.0. */
        response.setHeader("Expires", "0"); /* Proxies. */

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/authorize");
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", returnUri);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return "redirect:" + returnUri;
    }

    /*
     ** 쿠키 발급/재발급 요청
     */
    @RequestMapping(value = "/hq/oauth/token", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, Object> getAccessToken(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam("grant_type") String grant_type,
            @RequestParam("redirect_uri") Optional<String> redirect_uri,
            @RequestParam("code") Optional<String> code,
            @RequestParam("refresh_token") Optional<String> refresh_token
    ) {
        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/token (POST)");
        log_msg += String.format(":: %-20s = %s\n", "desc", "토큰 발급/재발급");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "grant_type", grant_type);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri.toString());
        log_msg += String.format(":: %-20s = %s\n", "code", code.toString());
        log_msg += String.format(":: %-20s = %s\n", "refresh_token", refresh_token.toString());
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        HashMap<String, Object> resultMap = null;

        try {
            resultMap = googleAuthService.requestPublishingTokens(grant_type,
                    redirect_uri.isPresent() ? redirect_uri.get() : null,
                    code.isPresent() ? code.get() : null,
                    refresh_token.isPresent() ? refresh_token.get() : null);
            /* 해당 코드를 찾을 수 없을 경우, 403 코드 반환 */
            if(resultMap == null) {
                log.error("# ERROR >> /oauth/token >> returned 403 ");
                response.sendError(403);
            }
        } catch (Exception e) {
            log.error("# ERROR >> /oauth/token >> exception >> {}", e.getMessage());

            try {
                response.sendError(500);
            } catch (Exception e1) {}
        }

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/token (POST)");
        log_msg += String.format(":: %-20s = %s\n", "refresh_token", refresh_token.toString());
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "result", (resultMap == null ? "-" : resultMap.toString()));
        log_msg += String.format(":: %-20s = %d\n", "response_code", response.getStatus());
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return resultMap;
    }

    /*
     ** 쿠키 발급/재발급 요청 + 사용자의 api ID/Key 전달
     */
    @RequestMapping(value = "/hq/oauth/token2", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, Object> getAccessTokenAndApiInfo(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam("grant_type") String grant_type,
            @RequestParam("redirect_uri") Optional<String> redirect_uri,
            @RequestParam("code") Optional<String> code,
            @RequestParam("refresh_token") Optional<String> refresh_token
    ) {
        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/token2 (POST)");
        log_msg += String.format(":: %-20s = %s\n", "desc", "토큰 발급/재발급 + user's API info");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "grant_type", grant_type);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri.toString());
        log_msg += String.format(":: %-20s = %s\n", "code", code.toString());
        log_msg += String.format(":: %-20s = %s\n", "refresh_token", refresh_token.toString());
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        HashMap<String, Object> tokenMap = null;

        try {
            tokenMap = googleAuthService.requestPublishingTokens(grant_type,
                    redirect_uri.isPresent() ? redirect_uri.get() : null,
                    code.isPresent() ? code.get() : null,
                    refresh_token.isPresent() ? refresh_token.get() : null);
            /* 해당 코드를 찾을 수 없을 경우, 403 코드 반환 */
            if(tokenMap == null) {
                log.error("# ERROR >> /oauth/token >> returned 403 ");
                response.sendError(403);
            }
        } catch (Exception e) {
            log.error("# ERROR >> /oauth/token >> exception >> {}", e.getMessage());

            try {
                response.sendError(500);
            } catch (Exception e1) {}
        }

        HashMap<String, Object> resultMap = googleAuthService.apiInfo(tokenMap);

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/token2 (POST)");
        log_msg += String.format(":: %-20s = %s\n", "refresh_token", refresh_token.toString());
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "result", (resultMap == null ? "-" : resultMap.toString()));
        log_msg += String.format(":: %-20s = %d\n", "response_code", response.getStatus());
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return resultMap;
    }

    /*
    ** 구글 로그인 콜백
    */
    @RequestMapping("/hq/oauth/callback")
    public String oauth2callback(HttpServletRequest request,
                                 HttpServletResponse response,
                                 @RequestParam("state") String state,
                                 @RequestParam("code") String code,
                                 @RequestParam(value = "channel", required = false, defaultValue = "google") String channel // google, facebook, maum
                                 ) throws UnsupportedEncodingException {
        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/callback");
        log_msg += String.format(":: %-20s = %s\n", "desc", "구글 로그인 콜백");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "code", code);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        String callbackUrl = null;

        callbackUrl = googleAuthService.procGoogleCallback(code, state);

        log.info("@ REDIRECT: {}", callbackUrl);

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/callback");
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", callbackUrl);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return "redirect:" + callbackUrl;
    }

    /*
     ** 로그인을 위한 권한 요청 (구글 로그인 수행)
     */
    @RequestMapping("/sso/oauth/authorize")
    public String ssoLogin(HttpServletRequest request,
                        HttpServletResponse response,
                        @RequestParam("response_type") String response_type,
                        @RequestParam("client_id")  String client_id,
                        @RequestParam("redirect_uri")  String redirect_uri,
                        @RequestParam("state")  String state
    ) {
        String codeMaumHQ = null;

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/authorize");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그인을 위한 권한 요청 (구글 로그인 수행)");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "response_type", response_type);
        log_msg += String.format(":: %-20s = %s\n", "client_id", client_id);
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", redirect_uri);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if(cookie.getName().equals(ConstDef.COOKIE_NAME___MAUM_HQ___STATE)) {
                    log.info("@ OAuth.token : Cookie.name={},{}", cookie.getName(), cookie.getValue());
                    codeMaumHQ = cookie.getValue();
                }
            }
        }

        String returnUri = googleAuthService.ssoAuthorize(response, codeMaumHQ, response_type, client_id, redirect_uri, state);

        /* 구글 인증 REDIRECTION URL 반환 */
        log.info("@ oauth/authorize : redirect_uri={}", returnUri);
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); /* HTTP 1.1. */
        response.setHeader("Pragma", "no-cache"); /* HTTP 1.0. */
        response.setHeader("Expires", "0"); /* Proxies. */

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/authorize");
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", returnUri);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return "redirect:" + returnUri;
    }

    /*
     ** 로그인 콜백, JWT
     */
    @RequestMapping("/sso/oauth/token")
    public String oauth2Token(HttpServletRequest request,
                                 HttpServletResponse response,
                                 @RequestParam("state") String state,
                                 @RequestParam("code") String code,
                                 @RequestParam(value = "channel", required = false, defaultValue = "google") String channel // google, facebook, maum
    ) {
        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/token");
        log_msg += String.format(":: %-20s = %s\n", "desc", "구글 로그인 콜백");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "code", code);
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        String callbackUrl = null;

        callbackUrl = googleAuthService.procSsoGoogleCallback(code, state);

        if(callbackUrl.contains(MAUM_AI___SIGNUP_PATH)) {
            log.debug("Skipping JWT cookie generation");
        } else {
            // 사용자의 결제 금액에 따른 사용 가능 서비스 조회
            OAuthTokenEntity oAuthTokenEntity = oAuthRepository.findByState(state);
            String email = oAuthTokenEntity.getEmail();
            int product = googleAuthService.getUserProduct(email);
            Map<String, Object> usableSrvMap = googleAuthService.getUsableServiceList(product, email);

            // Generate Access/Refresh-Token based on JWT(Json Web Token)
            String accessToken = jwtTokenUtil.generateAccessToken(email, usableSrvMap);
            String refreshToken = jwtTokenUtil.generateRefreshToken(email, usableSrvMap);

            // TODO: Store redis refresh-token
            redisTemplate.opsForValue().set(email + "_refresh_token", refreshToken);
            log.info(redisTemplate.opsForValue().get(email + "_refresh_token").toString());

            // Store Cookie for tokens(access/refresh) in ServletHttpResponse
            cookieUtil.generateCookieForJwtToken(accessToken, refreshToken, response);
        }
        log.info("@ REDIRECT: {}", callbackUrl);

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/token");
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", callbackUrl);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return "redirect:" + callbackUrl;
    }


    /*
    ** maum.ai 회원등록 콜백
    */
    @RequestMapping("/hq/oauth/signup-member-callback")
    public String signupMemberCallback(HttpServletRequest request,
                                 HttpServletResponse response,
                                 @RequestParam("state") String state
                                 ) throws UnsupportedEncodingException {

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/signup-member-callback");
        log_msg += String.format(":: %-20s = %s\n", "desc", "maum.ai 회원등록 콜백");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        String callbackUrl = googleAuthService.returnServiceCallback(state);

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/signup-member-callback");
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "redirect_uri", callbackUrl);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return "redirect:" + callbackUrl;
    }


    /* 로그아웃 요청 (deleteToken) - 2020. 08. 05 - LYJ */
    @RequestMapping(value = "/hq/oauth/deleteToken", method = RequestMethod.GET)
    public String logout(  HttpServletRequest request,
                           HttpServletResponse response,
                           @RequestParam("client_id") String client_id,
                           @RequestParam("access_token") String accessToken,
                           @RequestParam("returnUrl") String returnUrl
    ) throws ServletException {
        String result = null;

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/deleteToken (GET)");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그아웃 요청");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "client_id", client_id);
        log_msg += String.format(":: %-20s = %s\n", "access_token", accessToken);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        /* 쿠키 삭제 */
        Cookie hqCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, null);         //MaumHQ_State

        hqCookie.setMaxAge(0);
        hqCookie.setPath("/");
        hqCookie.setDomain("maum.ai");
        response.addCookie(hqCookie);

        Cookie theCookie = null;
        Cookie cookies[] = request.getCookies();

        if(cookies != null) {
            for(int i=0 ; i< cookies.length ; i++) {
                theCookie = cookies[i];
                if(theCookie.getName().equals(ConstDef.COOKIE_NAME___MAIN_JSESSION)) {  //MAIN_JSESSIONID
                    Cookie mainCookie = new Cookie(ConstDef.COOKIE_NAME___MAIN_JSESSION, null);

                    mainCookie.setMaxAge(0);
                    mainCookie.setPath("/");
                    mainCookie.setDomain("maum.ai");
                    response.addCookie(mainCookie);
                }
                if(theCookie.getName().equals(ConstDef.COOKIE_NAME___AIBUILDER_JSESSION)) {

                    Cookie builderCookie = new Cookie(ConstDef.COOKIE_NAME___AIBUILDER_JSESSION, null); //AIBUILDER_JSESSIONID

                    builderCookie.setMaxAge(0);
                    builderCookie.setPath("/");
                    builderCookie.setDomain("maum.ai");
                    response.addCookie(builderCookie);
                }
            }
        }

        try {
            googleAuthService.cleanTokens(client_id, accessToken);
            result = "succ";
        } catch (Exception e) {
            log.error("# /oauth/deleteToken (GET) >> Exception >> {}", e.getMessage());
            result = "exception: " + e.getMessage();
        }

        log.info("@ /oauth/deleteToken (GET) >> returned");

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/token (GET)");
        log_msg += String.format(":: %-20s = %s\n", "access_token", accessToken);
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "result", result);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        return "redirect:" + returnUrl;
    }

    /* 로그아웃 요청 (deleteToken) - 2020. 08. 05 - LYJ */
    @RequestMapping(value = "/sso/oauth/logout", method = RequestMethod.GET)
    public String logoutJwt( HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam("client_id") String client_id,
                             @RequestParam("returnUrl") String returnUrl
    ) {
        String result = null;

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/logout (GET)");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그아웃 요청");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "client_id", client_id);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        /* 쿠키 삭제 */
        Cookie hqCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, null);         //MaumHQ_State

        hqCookie.setMaxAge(0);
        hqCookie.setPath("/");
        hqCookie.setDomain("maum.ai");
        response.addCookie(hqCookie);

        Cookie theCookie = null;
        Cookie cookies[] = request.getCookies();

        if(cookies != null) {
            for(int i=0 ; i< cookies.length ; i++) {
                theCookie = cookies[i];
                if(theCookie.getName().contains(ConstDef.COOKIE_NAME___MAIN_JSESSION)) {  //MAIN_JSESSIONID

                    String cookieName = theCookie.getName();
                    Cookie mainCookie = new Cookie(cookieName, null);

                    mainCookie.setMaxAge(0);
                    mainCookie.setPath("/");
                    mainCookie.setDomain("maum.ai");
                    response.addCookie(mainCookie);
                }
                if(theCookie.getName().equals(ConstDef.COOKIE_NAME___AIBUILDER_JSESSION)) {

                    Cookie builderCookie = new Cookie(ConstDef.COOKIE_NAME___AIBUILDER_JSESSION, null); //AIBUILDER_JSESSIONID

                    builderCookie.setMaxAge(0);
                    builderCookie.setPath("/");
                    builderCookie.setDomain("maum.ai");
                    response.addCookie(builderCookie);
                }
                if(theCookie.getName().equals(ConstDef.MAUM_AID)) {

                    Cookie builderCookie = new Cookie(ConstDef.MAUM_AID, null); //MAUM_AID

                    builderCookie.setMaxAge(0);
                    builderCookie.setPath("/");
                    builderCookie.setDomain("maum.ai");
                    response.addCookie(builderCookie);
                }
                if(theCookie.getName().equals(ConstDef.MAUM_RID)) {

                    Cookie builderCookie = new Cookie(ConstDef.MAUM_RID, null); //MAUM_RID

                    builderCookie.setMaxAge(0);
                    builderCookie.setPath("/");
                    builderCookie.setDomain("maum.ai");
                    response.addCookie(builderCookie);
                }
            }
        }

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/logout (GET)");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "result", "success !");
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        return "redirect:" + returnUrl;
    }

    /*
    ** 로그아웃 요청
    */
    @RequestMapping(value = "/hq/oauth/token", method = RequestMethod.DELETE)
    @ResponseBody
    public String logout(  HttpServletRequest request,
                           HttpServletResponse response,
                           @RequestParam("client_id") String client_id,
                           @RequestParam("access_token") String accessToken
                           ) {
        String result = null;

        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/token (DELETE)");
        log_msg += String.format(":: %-20s = %s\n", "desc", "로그아웃 요청");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "client_id", client_id);
        log_msg += String.format(":: %-20s = %s\n", "access_token", accessToken);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);


        /* 쿠키 삭제 */
        Cookie myCookie = new Cookie(ConstDef.COOKIE_NAME___MAUM_HQ___STATE, null);
        response.addCookie(myCookie);

        try {
            googleAuthService.cleanTokens(client_id, accessToken);
            result = "succ";
        } catch (Exception e) {
            log.error("# /oauth/token (DELETE) >> Exception >> {}", e.getMessage());
            result = "exception: " + e.getMessage();
        }

        log.info("@ /oauth/token (DELETE) >> returned");

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/hq/oauth/token (DELETE)");
        log_msg += String.format(":: %-20s = %s\n", "access_token", accessToken);
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "result", result);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        return result;
    }

    /*
    **
    */
    @RequestMapping("/hq/hello")
    @ResponseBody
    public String hello(HttpServletRequest request
    ) {
        log.info("@ hello ...........................................  SID={} / RSID={}", request.getSession().getId(), request.getRequestedSessionId());
        if(request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                log.info("@ OAuth.token : Cookie.name={},{}", cookie.getName(), cookie.getValue());
            }
        }

        return "hi";
    }

}
