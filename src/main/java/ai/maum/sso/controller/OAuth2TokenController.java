package ai.maum.sso.controller;

import ai.maum.sso.common.data.CommonResponse;
import ai.maum.sso.dto.RequestRefreshTokenDto;
import ai.maum.sso.service.OAuth2TokenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@RestController
public class OAuth2TokenController {
    private final OAuth2TokenService oAuth2TokenService;

    @PostMapping("/sso/oauth/token:refresh")
    public ResponseEntity<String> refresh(@RequestBody @Valid RequestRefreshTokenDto requestRefreshTokenDto, BindingResult bindingResult) throws BindException {
        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/token:refresh");
        log_msg += String.format(":: %-20s = %s\n", "desc", "AccessToken 갱신");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "refresh_token", requestRefreshTokenDto.getRefresh_token());
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        String newAccessToken = oAuth2TokenService.refreshAccessToken(requestRefreshTokenDto.getRefresh_token());

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/sso/oauth/token:refresh");
        log_msg += String.format(":: %-20s = %s\n", "desc", "AccessToken 갱신");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "refresh_token", requestRefreshTokenDto.getRefresh_token());
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        return new ResponseEntity<>(newAccessToken, HttpStatus.OK);
    }

    @GetMapping("/oauth/validateCode")
    public CommonResponse<Object> validateCode(@RequestParam(value = "state") String state,
                                               @RequestParam(value = "code") String code) {
        String log_msg = "\n:: @ REQUEST\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/oauth/validateCode");
        log_msg += String.format(":: %-20s = %s\n", "desc", "state, code validation check");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "state", state);
        log_msg += String.format(":: %-20s = %s\n", "code", code);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        CommonResponse<Object> response = oAuth2TokenService.validateCode(state, code);

        log_msg = "\n:: @ RESPONSE\n";
        log_msg += ":: ====================================================================================================\n";
        log_msg += String.format(":: %-20s = %s\n", "uri_path", "/oauth/validateCode");
        log_msg += String.format(":: %-20s = %s\n", "desc", "state, code validation check");
        log_msg += ":: ----------------------------------------------------------------------------------------------------\n";
        log_msg += String.format(":: %-20s = %s\n", "response", response);
        log_msg += ":: ====================================================================================================\n";
        log.info(log_msg);

        return response;
    }
}
