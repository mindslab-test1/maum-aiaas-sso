package ai.maum.sso.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by kirk@mindslab.ai on 2020-09-17
 */

@Slf4j
@Controller
public class BasicErrorController implements ErrorController {

    private static final String ERROR_PATH = "/error";

    @Override
    public  String getErrorPath() {
        return "/error";
    }

    @RequestMapping(ERROR_PATH)
    public String handleError(HttpServletRequest request, Model model) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        HttpStatus httpStatus = HttpStatus.valueOf(Integer.valueOf(status.toString()));
        log.error("httpStatus: " + httpStatus.toString());
        model.addAttribute("code", status.toString());
        model.addAttribute("msg", httpStatus.toString());
        model.addAttribute("timestamp", new Date());

        return "/error";
    }
}
