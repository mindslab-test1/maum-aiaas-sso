package ai.maum.sso.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by kirk@mindslab.ai on 2021-01-11
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDto implements Serializable {
    private String name;
    private String email;
}
