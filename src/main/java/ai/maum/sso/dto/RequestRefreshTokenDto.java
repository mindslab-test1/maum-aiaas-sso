package ai.maum.sso.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class RequestRefreshTokenDto {
    @NotBlank
    private String refresh_token;
}
